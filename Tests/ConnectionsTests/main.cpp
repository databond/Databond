#include <Connections/ConnectionError.hpp>
#include <Connections/DBConnection.hpp>
#include <QCoreApplication>
#include <QObject>
#include <QSignalSpy>
#include <QSqlQuery>
#include <QTest>

using namespace Utilities;

class ConnectionsTests : public QObject {
  Q_OBJECT

public:
  ConnectionsTests(QObject *parent = nullptr) : QObject{parent} {}

private:
  Connections::DBConnection *createConnection(const QString &pluginName,
                                              const QString &conName) {
    return new Connections::DBConnection{pluginName, conName};
  }

  Utilities::UnitResult<Connections::ConnectionError> open() {
    const auto o = con->open();
    (con->exec(R"(
CREATE TABLE "SCHOOLS" (
    "NAME"	TEXT NOT NULL,
    PRIMARY KEY("NAME")
);
)"));
    return o;
  }
private slots:
  void init() {
    con = createConnection("BondSQLITE", QStringLiteral("1"));
    con->setDatabaseName(":memory:");
  }
  void cleanup() { Connections::safeRemove(con); }
  void test_isOpenExpectsFalse() { QVERIFY(!(con->isOpen())); }
  void test_isOpenExpectsTrue() {
    QSignalSpy spy{con, &Connections::Connection::connectionOpened};
    QVERIFY(open().has_value());
    const auto isO = (con->isOpen());
    QVERIFY(isO);
    QCOMPARE(spy.count(), 1);
  }
  void test_transactionExpectsError() {
    const auto t = (con->transaction());
    QVERIFY(!t.has_value());
    QCOMPARE(t.error(), Utilities::ErrorType::ConnectionNotOpened);
  }
  void test_transactionExpectsOperationFailedError() {
    open();
    (con->transaction());
    const auto t = (con->transaction());
    QVERIFY(!t.has_value());
    QCOMPARE(t.error(), Utilities::ErrorType::OperationFailed);
  }
  void test_transaction() {
    open();
    QSignalSpy spy{con, &Connections::Connection::connectionTransactionBegan};
    QVERIFY((con->transaction()).has_value());
    QCOMPARE(spy.count(), 1);
  }
  void test_rollbackExpectsOperationFailedError() {
    open();
    const auto r = (con->rollback());
    QVERIFY(!r.has_value());
    QCOMPARE(r.error(), Utilities::ErrorType::OperationFailed);
  }
  void test_rollback() {
    open();
    con->transaction();
    QSignalSpy spy{con, &Connections::Connection::connectionRollback};
    QVERIFY((con->rollback()).has_value());
    QCOMPARE(spy.count(), 1);
  }
  void test_commitExpectsOperationFailedError() {
    open();
    const auto c = (con->commit());
    QVERIFY(!c.has_value());
    QCOMPARE(c.error(), Utilities::ErrorType::OperationFailed);
  }
  void test_commit() {
    open();
    (con->transaction());
    QSignalSpy spy{con, &Connections::Connection::connectionCommited};
    QVERIFY((con->commit()).has_value());
    QCOMPARE(spy.count(), 1);
  }
  void test_execExpectsFalse() {
    const auto res = (con->exec("dad"));
    QVERIFY(!res.has_value());
    QCOMPARE(res.error(), ErrorType::ConnectionNotOpened);
  }
  void test_execFromWrongQueryExpectsOperationFailedError() {
    open();
    const auto res = (con->exec("INSERT INT SCHOOLS VALUES('Mohammed')"));
    QVERIFY(!res.has_value());
    QCOMPARE(res.error(), ErrorType::OperationFailed);
  }
  void test_execExpectsTrue() {
    open();
    const auto res = (con->exec("INSERT INTO SCHOOLS VALUES('Mohammed')"));
    QVERIFY(res.has_value());
    QCOMPARE(res.value().toJsonObject(),
             (QJsonObject{{"Query", "INSERT INTO SCHOOLS VALUES('Mohammed')"},
                          {"Result", true}}));
  }
  void test_execExpectsQueryResult() {
    open();
    con->exec("INSERT INTO SCHOOLS VALUES('Mohammed')");
    const auto res = (con->exec("SELECT * FROM SCHOOLS"));
    QVERIFY(res.has_value());
    QCOMPARE(res.value().toJsonObject(),
             (QJsonObject{
                 {"Query", "SELECT * FROM SCHOOLS"},
                 {"Result", QJsonObject{{"NAME", QJsonArray{"Mohammed"}}}}}));
  }

private:
  Connections::DBConnection *con;
  const char *mError;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  ConnectionsTests tests;
  QTest::qExec(&tests);
  return a.exec();
}

#include "main.moc"
