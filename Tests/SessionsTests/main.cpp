#include <Connections/Connection.hpp>
#include <Control/Client.hpp>
#include <QBuffer>
#include <QCoreApplication>
#include <QObject>
#include <QSignalSpy>
#include <QTemporaryFile>
#include <QTest>
#include <Sessions/Session.hpp>
using namespace Sessions;
using namespace Utilities;
class DummyConnection : public Connections::Connection {

public:
  DummyConnection(const QString &, const QString &name) { this->name = name; }
  // Connection interface
public:
  virtual void close() override { emit connectionClosed("con"); }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  commit() override {

    emit connectionCommited("con");
    return Utilities::Unit{};
  }
  virtual QString connectionOptions() override { return ""; }
  virtual QString connectionName() override { return name; }
  virtual QString databaseName() override { return "Db"; }
  virtual Utilities::Result<QVariant, Connections::ConnectionError>
  exec(const QString &query) override {
    return QVariant{};
  }
  virtual QString hostName() override { return "Host"; }
  virtual bool isOpen() override { return true; }
  virtual bool isOpenError() override { return false; }
  virtual bool isValid() override { return true; }
  virtual QSqlError lastSqlError() override { return QSqlError{}; }
  virtual QSql::NumericalPrecisionPolicy numericalPrecisionPolicy() override {
    return QSql::NumericalPrecisionPolicy::HighPrecision;
  }
  virtual Utilities::UnitResult<Connections::ConnectionError> open() override {
    return Utilities::Unit{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  open(const QString &username, const QString &password) override {
    return Utilities::Unit{};
  }
  virtual QString password() override { return "Pas"; }
  virtual int port() override { return 1; }
  virtual Utilities::Result<QSqlIndex, Connections::ConnectionError>
  primaryIndex(const QString &tableName) override {
    return QSqlIndex{};
  }
  virtual Utilities::Result<QSqlRecord, Connections::ConnectionError>
  record(const QString &tableName) override {
    return QSqlRecord{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  rollback() override {
    return Unit{};
  }
  virtual void setConnectionOptions(const QString &option) override {}
  virtual void setDatabaseName(const QString &name) override {}
  virtual void setHostName(const QString &name) override {}
  virtual void setNumericalPrecisionPolicy(
      QSql::NumericalPrecisionPolicy precisionPolicy) override {}
  virtual void setPassword(const QString &password) override {}
  virtual void setPort(const int port) override {}
  virtual void setUsername(const QString &username) override {}
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  tables(const QString &type) override {
    return QStringList{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  transaction() override {
    return Unit{};
  }
  virtual QString username() override { return "Usern"; }
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  columns(const QString &tableName) override {
    return QStringList{};
  }
  virtual Utilities::Result<QList<QPair<QString, QString>>,
                            Connections::ConnectionError>
  columnsWithTypes(const QString &tableName) override {
    return {};
  }
  virtual QStringList types() override { return QStringList{}; }
  virtual QString dbms() override { return "Dbms"; }
  virtual QStringList availableOptions() override { return QStringList{}; }
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  triggers() override {
    return QStringList{};
  }
  virtual QSqlDatabase database() override { return QSqlDatabase{}; }
  virtual Utilities::Result<QList<QPair<QString, QString>>,
                            Connections::ConnectionError>
  foreignKeys(const QString &tableName) override {
    return {};
  }
  virtual QJsonObject toJson() override {
    return {
        {
            "ConnectionName",
            QStringLiteral("%1.%2.%3").arg(1).arg(0).arg(name),
        },
        {
            "DatabaseName",
            "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\test.db",
        },
        {
            "HostName",
            "",
        },
        {
            "Options",
            "",
        },
        {
            "Password",
            "",
        },
        {
            "PluginName",
            "BondSQLITE",
        },
        {
            "Port",
            0,
        },
        {"Username", ""},
    };
  }
  QString name;
};

class SessionsTests : public QObject {
  Q_OBJECT
private slots:
  void init() { s = new Session{1, this}; }
  void cleanup() { s->deleteLater(); }

  void test_fromInvalidSessionJsonExpectsError() {
    auto json = QStringLiteral(R"(
{
    "Connectios": [
    ],
    "Name": "BondSqlite"
}
)")
                    .toUtf8();
    QBuffer buf{&json};
    auto s = fromSource<DummyConnection>(1, buf);
    QVERIFY(!s.has_value());
    QCOMPARE(s.error(), Utilities::ErrorType::SourceError);
  }
  void test_fromInvalidConnectionJsonExpectsOnlyValidSession() {
    auto json = QStringLiteral(R"(
{
    "Connections": [
        {
            "ConnetionName": "Sqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\test.db",
            "HostName": "",
            "Options": "",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        }
    ],
    "Name": "BondSqlite"
}
)")
                    .toUtf8();
    QBuffer buf{&json};
    const auto s = fromSource<DummyConnection>(1, buf);
    QVERIFY(s.has_value());

    QCOMPARE(s.value()->connectionsCount(), 0);
    s.value()->deleteLater();
  }
  void test_fromPartiallyValidConnectionJsonExpectsOneConnection() {
    auto json = QStringLiteral(R"(
{
    "Connections": [
        {
            "ConnectionName": "Sqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\test.db",
            "HostName": "",
            "Options": "",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        },
        {
            "ConnectionName": "ComplexSqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\chinook.db",
            "HostName": "",
            "Options": "QSQLITE_ENABLE_REGEXP=ON",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0
        }
    ],
    "Name": "BondSqlite"
}
)")
                    .toUtf8();
    QBuffer buffer{&json};
    const auto s = fromSource<DummyConnection>(1, buffer);
    QVERIFY(s.has_value());
    QCOMPARE(s.value()->connectionsCount(), 1);
    s.value()->deleteLater();
  }
  void test_fromValidJsonWithConnections() {

    auto json = QStringLiteral(R"(
{
    "Connections": [
        {
            "ConnectionName": "Sqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\test.db",
            "HostName": "",
            "Options": "",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        },
        {
            "ConnectionName": "ComplexSqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\chinook.db",
            "HostName": "",
            "Options": "QSQLITE_ENABLE_REGEXP=ON",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        }
    ],
    "Name": "BondSqlite"
}
)")
                    .toUtf8();
    QBuffer buffer{&json};
    auto session = Sessions::fromSource<DummyConnection>(1, buffer);
    QVERIFY(session.has_value());
    QCOMPARE(session.value()->connectionsCount(), 2);
    session.value()->deleteLater();
  }
  void test_saveSession() {
    QBuffer buffer;
    auto con1 = new DummyConnection{"p", "con"},
         con2 = new DummyConnection{"p", "con1"};
    s->addConnection(con1);
    s->addConnection(con2);
    const auto expected = s->toJson();
    const auto res = s->save(buffer);

    buffer.close();
    buffer.open(QIODevice::ReadOnly | QIODevice::Text);
    const auto doc = QJsonDocument::fromJson(buffer.readAll()).object();

    QVERIFY(res.has_value());
    QCOMPARE(doc, expected);
  }

private:
  Sessions::Session *s;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  SessionsTests sessionsTests;
  QTest::qExec(&sessionsTests);
  return a.exec();
}
#include "main.moc"
