#include <MessagePackRpc/MessagePackRpcServer.hpp>
#include <MessagePackRpc/Types.hpp>
#include <QCoreApplication>
#include <QObject>
#include <QTest>
#include <exception>
#include <rpc/client.h>
#include <rpc/rpc_error.h>

using namespace rpc;
using namespace Service;
class MessagePacksTestsRpc : public QObject {
  Q_OBJECT
private slots:
  void initTestCase_data() {
    mServer = new Service::MessagePackRpcServer{
        QString::fromStdString(localHost()), 8080};
  }
  void initTestCase() { mServer->start(4); }
  void test_newSession() {
    client c{localHost(), 8080};
    c.call("request");
    auto id = c.call("session.new").as<int>();
    QCOMPARE(id, 0);
  }

  void test_newSessionWithName() {
    client c{localHost(), 8080};
    c.call("request");
    auto id = c.call("session.newWithName", "mysession").as<int>();

    QCOMPARE(id, 0);
    auto name = c.call("session.name", id).as<std::string>();
    QCOMPARE(name, "mysession");
  }
  void test_exist() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    auto res = c.call("session.exist", 0).as<bool>();
    QVERIFY(res);
  }
  void test_close() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("session.close", 0);
    auto res = c.call("session.exist", 0).as<bool>();
    QVERIFY(!res);
  }
  void test_sessionCount() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("session.new");
    c.call("session.new");
    auto count = c.call("session.count").as<int>();
    QCOMPARE(count, 3);
  }
  void test_saveInfIle() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    auto res = c.call("session.file.save", 0, "test.bond").as<bool>();
    QVERIFY(res);
    QFile::remove("test.bond");
  }
  void test_safeInFileExpectsError() {
    try {
      client c{localHost(), 8080};
      c.call("request");
      c.call("session.new");
      c.call("session.saveInFile", 0, "test.bon").as<bool>();
    } catch (rpc::rpc_error er) {
      qDebug() << "Got an error, don't freak out, this is desired";
      QVERIFY(true);
    }
  }
  void test_addConnection() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con1");
    auto count = c.call("connection.count", 0).as<int>();

    QCOMPARE(count, 1);
  }
  void test_connectionCount() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con1");
    c.call("connection.addConnection", 0, "BondSQLITE", "con2");
    c.call("connection.addConnection", 0, "BondSQLITE", "con3");
    auto count = c.call("connection.count", 0).as<int>();
    QCOMPARE(count, 3);
  }
  void test_connectionOpen() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con1");
    auto res = c.call("connection.open", 0, "con1").as<bool>();
    QVERIFY(res);
  }
  void test_connectionTransaction() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.open", 0, "con");
    auto res = c.call("connection.transaction", 0, "con").as<bool>();
    QVERIFY(res);
  }
  void test_connectionRollback() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.open", 0, "con");
    c.call("connection.transaction", 0, "con");
    auto res = c.call("connection.rollback", 0, "con").as<bool>();
    QVERIFY(res);
  }
  void test_connectionCommit() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.open", 0, "con");
    c.call("connection.transaction", 0, "con");
    auto res = c.call("connection.commit", 0, "con").as<bool>();
    QVERIFY(res);
  }
  void test_connectionIsOpen() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.open", 0, "con");
    auto res = c.call("connection.isOpen", 0, "con").as<bool>();
    QVERIFY(res);
  }
  void test_connectionOptions() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setOptions", 0, "con", "bond=1;faisal=2");
    auto res = c.call("connection.options", 0, "con").as<std::string>();
    QCOMPARE(res, "bond=1;faisal=2");
  }
  void test_connectionDatabaseName() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setDatabaseName", 0, "con", "DB");
    auto res = c.call("connection.databaseName", 0, "con").as<std::string>();
    QCOMPARE(res, "DB");
  }
  void test_connectionHost() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setHostname", 0, "con", "localhost");
    auto res = c.call("connection.hostname", 0, "con").as<std::string>();
    QCOMPARE(res, "localhost");
  }
  void test_connectionUsername() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setUsername", 0, "con", "bond");
    auto res = c.call("connection.username", 0, "con").as<std::string>();
    QCOMPARE(res, "bond");
  }
  void test_connectionPassword() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setPassword", 0, "con", "1234");
    auto res = c.call("connection.password", 0, "con").as<std::string>();
    QCOMPARE(res, "1234");
  }
  void test_connectionPort() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setPort", 0, "con", 8080);
    auto res = c.call("connection.port", 0, "con").as<int>();
    QCOMPARE(res, 8080);
  }
  void test_connectionError() {
    try {
      client c{localHost(), 8080};
      c.call("request");
      c.call("session.new");
      c.call("connection.addConnection", 0, "BondSQLITE", "con");
      c.call("connection.commit", 0, "con").as<bool>();
    } catch (rpc::rpc_error er) {
      qDebug() << "Got an error, don't freak out, this is desired";
      QVERIFY(true);
    }
  }
  void test_connectionSqlError() {
    try {
      client c{localHost(), 8080};
      c.call("request");
      c.call("session.new");
      c.call("connection.addConnection", 0, "BondSQLITE", "con");
      c.call("connection.open", 0, "con");
      c.call("connection.commit", 0, "con").as<bool>();
    } catch (rpc::rpc_error er) {
      qDebug() << "Got an error, don't freak out, this is desired";
      QVERIFY(true);
    }
  }
  void test_connectionRemove() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.remove", 0, "con");
    auto res = c.call("connection.count", 0).as<int>();
    QCOMPARE(res, 0);
  }
  void test_exec() {
    client c{localHost(), 8080};
    c.call("request");
    c.call("session.new");
    c.call("connection.addConnection", 0, "BondSQLITE", "con");
    c.call("connection.setDatabaseName", 0, "con", ":memory:");
    c.call("connection.open", 0, "con");
    auto isCreated =
        c.call("connection.execute", 0, "con", R"(CREATE TABLE "SCHOOLS" (
    "NAME"	TEXT NOT NULL,
    PRIMARY KEY("NAME")
    ))")
            .as<std::string>();
    qDebug() << QString::fromStdString(isCreated);
    auto res = c.call("connection.execute", 0, "con", "SELECT * FROM SCHOOLS")
                   .as<std::string>();
    qDebug() << QString::fromStdString(res);
  }

  void cleanupTestCase() { mServer->deleteLater(); }

private:
  std::string localHost() { return "127.0.0.1"; }

private:
  Service::MessagePackRpcServer *mServer;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  MessagePacksTestsRpc tests;
  QTest::qExec(&tests);
  return a.exec();
}
#include "main.moc"
