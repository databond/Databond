#include <QCoreApplication>
#include <QObject>
#include <QTest>
#include <Sessions/SessionManager.hpp>
using namespace Sessions;
using namespace Utilities;
class DummyConnection : public Connections::Connection {

public:
  DummyConnection(const QString &, const QString &name) { this->name = name; }
  // Connection interface
public:
  virtual void close() override { emit connectionClosed("con"); }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  commit() override {

    emit connectionCommited("con");
    return Utilities::Unit{};
  }
  virtual QString connectionOptions() override { return ""; }
  virtual QString connectionName() override { return name; }
  virtual QString databaseName() override { return "Db"; }
  virtual Utilities::Result<QVariant, Connections::ConnectionError>
  exec(const QString &query) override {
    return QVariant{};
  }
  virtual QString hostName() override { return "Host"; }
  virtual bool isOpen() override { return true; }
  virtual bool isOpenError() override { return false; }
  virtual bool isValid() override { return true; }
  virtual QSqlError lastSqlError() override { return QSqlError{}; }
  virtual QSql::NumericalPrecisionPolicy numericalPrecisionPolicy() override {
    return QSql::NumericalPrecisionPolicy::HighPrecision;
  }
  virtual Utilities::UnitResult<Connections::ConnectionError> open() override {
    return Utilities::Unit{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  open(const QString &username, const QString &password) override {
    return Utilities::Unit{};
  }
  virtual QString password() override { return "Pas"; }
  virtual int port() override { return 1; }
  virtual Utilities::Result<QSqlIndex, Connections::ConnectionError>
  primaryIndex(const QString &tableName) override {
    return QSqlIndex{};
  }
  virtual Utilities::Result<QSqlRecord, Connections::ConnectionError>
  record(const QString &tableName) override {
    return QSqlRecord{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  rollback() override {
    return Unit{};
  }
  virtual void setConnectionOptions(const QString &option) override {}
  virtual void setDatabaseName(const QString &name) override {}
  virtual void setHostName(const QString &name) override {}
  virtual void setNumericalPrecisionPolicy(
      QSql::NumericalPrecisionPolicy precisionPolicy) override {}
  virtual void setPassword(const QString &password) override {}
  virtual void setPort(const int port) override {}
  virtual void setUsername(const QString &username) override {}
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  tables(const QString &type) override {
    return QStringList{};
  }
  virtual Utilities::UnitResult<Connections::ConnectionError>
  transaction() override {
    return Unit{};
  }
  virtual QString username() override { return "Usern"; }
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  columns(const QString &tableName) override {
    return QStringList{};
  }
  virtual Utilities::Result<QList<QPair<QString, QString>>,
                            Connections::ConnectionError>
  columnsWithTypes(const QString &tableName) override {
    return {};
  }
  virtual QStringList types() override { return QStringList{}; }
  virtual QString dbms() override { return "Dbms"; }
  virtual QStringList availableOptions() override { return QStringList{}; }
  virtual Utilities::Result<QStringList, Connections::ConnectionError>
  triggers() override {
    return QStringList{};
  }
  virtual QSqlDatabase database() override { return QSqlDatabase{}; }
  virtual Utilities::Result<QList<QPair<QString, QString>>,
                            Connections::ConnectionError>
  foreignKeys(const QString &tableName) override {
    return {};
  }
  virtual QJsonObject toJson() override { return {}; }
  QString name;
};

struct TemporaryFile {
  TemporaryFile(const QString &filename) : file{filename} {
    file.open(QIODevice::ReadWrite);
  }

  ~TemporaryFile() { file.remove(); }

  QFile file;
};

using namespace Utilities;
using namespace Control;
class SessionManagerTests : public QObject {
  Q_OBJECT
private slots:
  void init() { s = new SessionManager{"123"}; }
  void cleanup() { s->deleteLater(); }
  void test_newSession() {
    s->newSession();
    QCOMPARE(s->count(), 1);
    QVERIFY(s->exists(0));
  }
  void test_closeSession() {
    s->newSession();
    s->close(0);
    QCOMPARE(s->count(), 0);
    QVERIFY(!s->exists(0));
  }
  void test_newSessionFromFile() {
    TemporaryFile tempFile{"TestSession.bond"};
    const auto json = QStringLiteral(R"(
{
    "Connections": [
        {
            "ConnectionName": "Sqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\test.db",
            "HostName": "",
            "Options": "",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        },
        {
            "ConnectionName": "ComplexSqlite",
            "DatabaseName": "C:\\Users\\Bond\\Desktop\\Playground\\TestDb\\Sqlite\\chinook.db",
            "HostName": "",
            "Options": "QSQLITE_ENABLE_REGEXP=ON",
            "Password": "",
            "PluginName": "BondSQLITE",
            "Port": 0,
            "Username": ""
        }
    ],
    "Name": "BondSqlite"
}
)")
                          .toUtf8();
    tempFile.file.write(json);

    const auto filename = tempFile.file.fileName();
    tempFile.file.close();
    s->newSessionFromFile<DummyConnection>(filename);
    QVERIFY(s->exists(0));
    QCOMPARE(s->count(), 1);
  }
  void test_newSessionFromFileWithWrongSuffixExpectsError() {
    TemporaryFile file{"bond.te"};
    auto session = s->newSessionFromFile<DummyConnection>(file.file.fileName());
    QVERIFY(!session.has_value());
    QCOMPARE(session.error(), ErrorType::SourceError);
  }

private:
  SessionManager *s;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  SessionManagerTests tests;

  QTest::qExec(&tests);
  return a.exec();
}

#include "main.moc"
