set(SOURCES main.cpp ${CMAKE_SOURCE_DIR}/Src/Json.qrc)
add_executable(SessionManagerTests ${SOURCES})
target_link_libraries(SessionManagerTests Qt${QT_VERSION_MAJOR}::Test Qt${QT_VERSION_MAJOR}::Core Sessions)
