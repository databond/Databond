#include <Connections/ConnectionManager.hpp>
#include <Control/Client.hpp>
#include <Control/ClientManager.hpp>
#include <QCoreApplication>
#include <QObject>
#include <QTest>
#include <Sessions/SessionManager.hpp>

using namespace Utilities;
using namespace Control;
using namespace Sessions;
using namespace Connections;

class ClientTests : public QObject {
  Q_OBJECT
private slots:
  void init() { id = ClientManager::request().value(); }
  void cleanup() { ClientManager::remove(id); }
  void test_sessionManager() {
    auto client = ClientManager::client(id).value();
    client->call(QOverload<>::of(&SessionManager::newSession));
    client->call(QOverload<>::of(&SessionManager::newSession));
    QCOMPARE(client->call(&SessionManager::count), 2);
    QVERIFY(client->call(&SessionManager::exists, 0));
    client->call(&SessionManager::close, 0);
    QVERIFY(!client->call(&SessionManager::exists, 0));
    QCOMPARE(client->call(&SessionManager::count), 1);
  }
  void test_connectionManager() {
    auto client = ClientManager::client(id).value();
    client->call(QOverload<>::of(&SessionManager::newSession));
    client->call(QOverload<int, const QString &, const QString &>::of(
                     &ConnectionManager::addConnection),
                 0, "BondSQLITE", "1");
    client->call(QOverload<int, const QString &, const QString &>::of(
                     &ConnectionManager::addConnection),
                 0, "BondSQLITE", "2");
    QCOMPARE(client->call(&ConnectionManager::count, 0), 2);
  }

private:
  QString id;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  ClientTests tests;

  QTest::qExec(&tests);

  return a.exec();
}

#include "main.moc"
