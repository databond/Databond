#include <Connections/ConnectionManager.hpp>
#include <QCoreApplication>
#include <QObject>
#include <QSignalSpy>
#include <QTest>
#include <Sessions/SessionManager.hpp>

using namespace Connections;
using namespace Sessions;
using namespace Utilities;

class ConnectionManagerTests : public QObject {
  Q_OBJECT
private slots:
  void init() {
    s = new SessionManager{id};
    c = new ConnectionManager{id, s};
    s->newSession();
    QVERIFY(s->exists(0));
    c->addConnection(0, "BondSQLITE", "1");
    c->setDatabaseName(0, "1", ":memory:");
    QCOMPARE(c->count(0), 1);
  }
  void cleanup() {
    c->deleteLater();
    s->deleteLater();
  }
  void test_addConnection() {
    c->addConnection(0, "BondSQLITE", "2");
    QCOMPARE(c->count(0), 2);
  }
  void test_addConnectionFromInvalidPlugin() {
    c->addConnection(0, "InvalidPlugin", "2");
    QCOMPARE(c->count(0), 1);
  }
  void test_isOpenExpectsFalse() {
    auto isO = c->isOpen(0, "1");
    QVERIFY(isO.has_value());
    QVERIFY(!isO.value());
  }
  void test_open() {
    QSignalSpy spy{c, &ConnectionManager::connectionOpened};
    c->open(0, "1");
    QVERIFY(c->isOpen(0, "1").value());
    QCOMPARE(spy.count(), 1);
  }

  void test_close() {
    c->open(0, "1");
    QSignalSpy spy(c, &ConnectionManager::connectionClosed);
    c->close(0, "1");
    QVERIFY(!c->isOpen(0, "1").value());
    QCOMPARE(spy.count(), 1);
  }
  void test_transactionExpectsError() {
    auto t = c->transaction(0, "1");
    QVERIFY(!t.has_value());
    QCOMPARE(t.error(), ErrorType::ConnectionNotOpened);
  }
  void test_transaction() {
    QSignalSpy spy{c, &ConnectionManager::connectionTransactionBegan};
    c->open(0, "1");
    const auto res = c->transaction(0, "1");
    QVERIFY(res.has_value());
    QCOMPARE(spy.count(), 1);
  }
  void test_rollbackExpectsError() {
    c->open(0, "1");
    auto r = c->rollback(0, "1");
    QVERIFY(!r.has_value());
    QCOMPARE(r.error(), ErrorType::OperationFailed);
  }
  void test_rollback() {
    QSignalSpy spy{c, &ConnectionManager::connectionRollback};
    c->open(0, "1");
    c->transaction(0, "1");
    auto r = c->rollback(0, "1");
    QVERIFY(r.has_value());
    QCOMPARE(spy.count(), 1);
  }
  void test_commitExpectsError() {
    c->open(0, "1");
    auto commit = c->commit(0, "1");
    QVERIFY(!commit.has_value());
    QCOMPARE(commit.error(), ErrorType::OperationFailed);
  }
  void test_commit() {
    QSignalSpy spy{c, &ConnectionManager::connectionCommited};
    c->open(0, "1");
    c->transaction(0, "1");
    auto r = c->commit(0, "1");
    QVERIFY(r.has_value());
    QCOMPARE(spy.count(), 1);
  }

private:
  QString id = "123";
  SessionManager *s;
  ConnectionManager *c;
};

auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  ConnectionManagerTests tests;

  QTest::qExec(&tests);
  return a.exec();
}

#include "main.moc"
