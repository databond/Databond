#include "Session.hpp"
#include <Connections/Connection.hpp>
#include <Connections/ConnectionManager.hpp>
#include <Connections/DBConnection.hpp>
#include <Control/Client.hpp>
#include <Utilities/Json.hpp>
namespace Sessions {
using namespace Utilities;

Session::Session(const int sessionId, QObject *parent)
    : QObject(parent), mId{sessionId} {}

Session::~Session() {
  for (auto con : mConnections) {
    Connections::safeRemove(con);
  }
  mConnections.clear();
}

int Session::id() const noexcept { return mId; }

void Session::setName(const QString &name) { mName = name; }

QString Session::name() const noexcept { return mName; }
QStringList Session::connectionNames() const {
  QStringList names;
  for (const auto con : mConnections) {
    names.push_back(con->connectionName());
  }
  return names;
}

void Session::addConnection(Connections::Connection *con) {
  mConnections.push_back(con);
}

QList<Connections::Connection *> Session::connections() const noexcept {
  return mConnections;
}

int Session::connectionsCount() const noexcept { return mConnections.size(); }

void Session::removeConnection(const QString &name) {
  if (auto con = connectionByName(name); con.has_value()) {
    mConnections.removeOne(con.value());
    Connections::safeRemove(con.value());
  }
}

Utilities::UnitResult<SessionError> Session::save(QIODevice &source) {
  const auto doc = QJsonDocument::fromVariant(toJson().toVariantMap());
  if (source.open(QIODevice::WriteOnly | QIODevice::Text) &&
      source.write(doc.toJson(QJsonDocument::Indented)) != -1) {
    return Unit{};
  }
  return Err{SessionError{ErrorType::SourceError,
                          QStringLiteral("Could not write on source, %1")
                              .arg(source.errorString())}};
}

Utilities::Result<Connections::Connection *, Connections::ConnectionError>
Session::connectionByName(const QString &name) {
  for (const auto con : qAsConst(mConnections)) {
    if (con->connectionName() == name) {
      return con;
    }
  }
  return Err{Connections::ConnectionError{
      ErrorType::ConnectionNotFound,
      QStringLiteral("Connection %1 not found").arg(name)}};
}

QJsonObject Session::toJson() const {
  QJsonObject sessionObj;
  sessionObj["Name"] = mName;
  QJsonArray connections;
  for (const auto &con : mConnections) {
    auto conObj = con->toJson();
    conObj["ConnectionName"] =
        Namer::getConnectionNamePart(conObj["ConnectionName"].toString())
            .value();
    connections.push_back(conObj);
  }
  sessionObj["Connections"] = connections;
  return sessionObj;
}

} // namespace Sessions
