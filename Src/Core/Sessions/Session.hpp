#ifndef SESSIONS_SESSION_HPP
#define SESSIONS_SESSION_HPP

#include "SessionError.hpp"
#include <Connections/DBConnection.hpp>
#include <QIODevice>
#include <QObject>
#include <Utilities/Json.hpp>
#include <Utilities/Namer.hpp>
#include <Utilities/expected.hpp>
namespace Control {
class Client;
}
namespace Connections {
class Connection;
class DBConnection;
class ConnectionManager;
} // namespace Connections
namespace Sessions {

class Session : public QObject {
  Q_OBJECT
public:
  explicit Session(const int sessionId, QObject *parent = nullptr);

  ~Session();

public:
  int id() const noexcept;
  void setName(const QString &name);
  QString name() const noexcept;
  QStringList connectionNames() const;
  void addConnection(Connections::Connection *con);
  template <typename ConnectionType = Connections::DBConnection>
  Utilities::Result<ConnectionType *, Connections::ConnectionError>
  addConnection(const QJsonObject &conObj) {
    if (auto con = Connections::fromJson<ConnectionType>(conObj); con) {
      addConnection(con.value());
      return con.value();
    } else {
      return Utilities::Err{con.error()};
    }
  }
  template <typename ConnectionType = Connections::DBConnection>
  Utilities::Result<ConnectionType *, Connections::ConnectionError>
  addConnection(const QString &pluginName, const QString &conName) {
    auto con = new ConnectionType{pluginName, conName};
    auto er = con->lastConnectionError();
    if (er == Utilities::ErrorType::PluginError) {
      Connections::safeRemove(con);
      return Utilities::Err{er};
    }
    addConnection(con);
    return con;
  }
  QList<Connections::Connection *> connections() const noexcept;

  int connectionsCount() const noexcept;

  void removeConnection(const QString &name);

  Utilities::UnitResult<SessionError> save(QIODevice &source);
  Utilities::Result<Connections::Connection *, Connections::ConnectionError>
  connectionByName(const QString &name);

  QJsonObject toJson() const;

private:
  SessionError mLastError;
  int mId;
  QString mName;
  QList<Connections::Connection *> mConnections;
};
template <typename ConnectionType = Connections::DBConnection>
Utilities::Result<Session *, SessionError>
fromSource(const int id, QIODevice &source, const QString &identifier = "",
           QObject *parent = nullptr) {
  if (!source.isOpen()) {
    if (!source.open(QIODevice::ReadOnly)) {
      return Utilities::Err{
          SessionError{Utilities::ErrorType::SourceError,
                       QStringLiteral("Failed to open the source, %1")
                           .arg(source.errorString())}};
    }
  }
  const auto readJson = QJsonDocument::fromJson(source.readAll()).object();
  if (!Utilities::Json::validateSessionSchema(readJson)) {
    return Utilities::Err{SessionError{
        Utilities::ErrorType::SourceError,
        QStringLiteral("Failed to create a session from an invalid json "
                       "source, the source must look like this: %1")
            .arg(QString::fromUtf8(
                QJsonDocument(Utilities::Json::makeSessionJsonTemplate())
                    .toJson(QJsonDocument::Indented)))}};
  }
  auto session = new Session{id, parent};
  session->setName(readJson["Name"].toString());
  for (const auto &conObj : readJson["Connections"].toArray()) {
    Utilities::Namer::makeUniqueConnectionName(
        identifier, id, conObj.toObject()["ConnectionName"].toString())
        .map([&conObj, session](const QString &name) {
          auto obj = conObj.toObject();
          obj["ConnectionName"] = name;
          session->addConnection<ConnectionType>(obj);
        })
        .map_error([session, &conObj](const auto) {
          session->addConnection<ConnectionType>(conObj.toObject());
        });
  }
  return session;
}

} // namespace Sessions

#endif // SESSIONS_SESSION_HPP
