#ifndef SESSIONS_SESSIONERROR_HPP
#define SESSIONS_SESSIONERROR_HPP

#include <Utilities/Error.hpp>

namespace Sessions {

class SessionError : public Utilities::Error {
public:
  SessionError();
  SessionError(const SessionError &);
  SessionError(SessionError &&);
  SessionError(const Utilities::ErrorType type, const QString &msg);

public:
  auto operator=(const SessionError &) -> SessionError &;
  auto operator=(SessionError &&) -> SessionError &;
  // Error interface
public:
  virtual QString explain() const noexcept override;

  // Error interface
public:
  virtual bool isSessionError() const noexcept override;
  virtual QString errorTag() const noexcept override;
};

} // namespace Sessions
Q_DECLARE_METATYPE(Sessions::SessionError)
#endif // SESSIONS_SESSIONERROR_HPP
