#include "SessionError.hpp"

namespace Sessions {

SessionError::SessionError() = default;

SessionError::SessionError(const Utilities::ErrorType type, const QString &msg)
    : Utilities::Error{type, msg} {}

SessionError &SessionError::operator=(const SessionError &) = default;
SessionError &SessionError::operator=(SessionError &&) = default;

SessionError::SessionError(SessionError &&) = default;

SessionError::SessionError(const SessionError &) = default;

QString SessionError::explain() const noexcept {
  return QStringLiteral("Session Error: %1").arg(Error::explain());
}

} // namespace Sessions

bool Sessions::SessionError::isSessionError() const noexcept { return true; }

QString Sessions::SessionError::errorTag() const noexcept {
  return "SessionError";
}
Utilities::ErrorVariant::ErrorVariant(const Sessions::SessionError &er)
    : QVariant{QVariant::fromValue(er)} {}
auto Utilities::ErrorVariant::operator=(
    const Sessions::SessionError &er) noexcept -> ErrorVariant & {
  setValue(fromValue<Sessions::SessionError>(er));
  return *this;
}
bool Utilities::ErrorVariant::isSessionError() const noexcept {
  return canConvert<Sessions::SessionError>();
}
