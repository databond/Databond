#ifndef SESSIONS_SESSIONMANAGER_HPP
#define SESSIONS_SESSIONMANAGER_HPP

#include "Session.hpp"
#include "SessionError.hpp"
#include <QObject>
#include <Utilities/expected.hpp>

namespace Connections {
class DBConnection;
class ConnectionManager;
} // namespace Connections

namespace Sessions {
class SessionManager : public QObject {
  Q_OBJECT
public:
  SessionManager(const QString &clientId, QObject *parent = nullptr);
  ~SessionManager();
  int newSession();
  QList<QPair<int, QString>> sessions();
  int newSession(const QString &name);
  bool exists(const int id) const noexcept;
  Utilities::Result<QString, SessionError> name(const int id) const noexcept;
  void setName(const int id, const QString &name);
  int count() const noexcept;
  Utilities::UnitResult<SessionError> saveInFile(const int id,
                                                 const QString &filename);
  template <typename ConnectionType = Connections::DBConnection>
  Utilities::Result<int, SessionError>
  newSessionFromFile(const QString &filename,
                     Connections::ConnectionManager *cm = nullptr) {
    using namespace Utilities;
    QFileInfo info{filename};
    if (info.suffix() != "bond") {
      return Err{SessionError{Utilities::ErrorType::SourceError,
                              "Sessions files must have '.bond' suffix"}};
    }
    QFile file{filename};
    const auto id = newSessionId();
    auto s = fromSource<ConnectionType>(id, file, mClientId);
    if (s.has_value()) {
      mSessions.push_back(s.value());
      emit sessionCreated(id);
      if (cm) {
        connectionToConnectionManager(s.value(), cm);
      }
      return id;
    } else {
      return Utilities::Err{s.error()};
    }
  }
  void close(const int id);

  Utilities::Result<Session *, SessionError>
  sessionById(const int id) const noexcept;
signals:
  void sessionCreated(int);
  void sessionClosed(int);
  void nameChanged(int, QString);

private:
  int newSessionId();
  void connectionToConnectionManager(const Session *s,
                                     Connections::ConnectionManager *cm);

private:
  QString mClientId;
  QList<Session *> mSessions;
};

} // namespace Sessions

#endif // SESSIONS_SESSIONMANAGER_HPP
