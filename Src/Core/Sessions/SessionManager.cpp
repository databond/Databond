#include "SessionManager.hpp"
#include <Connections/ConnectionManager.hpp>
namespace Sessions {
using namespace Utilities;

SessionManager::SessionManager(const QString &clientId, QObject *parent)
    : QObject{parent}, mClientId{clientId} {}

SessionManager::~SessionManager() {
  for (auto s : qAsConst(mSessions)) {
    delete s;
  }
  mSessions.clear();
}
QList<QPair<int, QString>> SessionManager::sessions() {
  QList<QPair<int, QString>> s;
  for (const auto &session : mSessions) {
    s.push_back({session->id(), session->name()});
  }
  return s;
}
int SessionManager::newSession() {
  const auto id = newSessionId();
  auto s = new Session{id, nullptr};
  emit sessionCreated(id);
  mSessions.push_back(s);
  return id;
}

int SessionManager::newSession(const QString &name) {
  auto id = newSession();
  sessionById(id).value()->setName(name);
  return id;
}

bool SessionManager::exists(const int id) const noexcept {
  return sessionById(id).has_value();
}

Utilities::Result<QString, SessionError>
SessionManager::name(const int id) const noexcept {
  if (auto s = sessionById(id); s.has_value()) {
    return s.value()->name();
  }
  return Err{SessionError{ErrorType::SessionNotFound,
                          QStringLiteral("Session with %1 not found")}};
}

void SessionManager::setName(const int id, const QString &name) {
  if (auto s = sessionById(id); s.has_value()) {
    if (s.value()->name() != name) {
      s.value()->setName(name);
      emit nameChanged(id, name);
    }
  }
}

int SessionManager::count() const noexcept { return mSessions.count(); }

UnitResult<SessionError> SessionManager::saveInFile(const int id,
                                                    const QString &filename) {
  if (QFileInfo{filename}.suffix() != "bond") {
    return Err{SessionError{ErrorType::SourceError,
                            "Sessions files must have '.bond' suffix"}};
  }
  if (auto s = sessionById(id); s.has_value()) {
    QFile file{filename};
    return s.value()->save(file);
  } else {
    return Err{SessionError{s.error()}};
  }
}

void SessionManager::close(const int id) {
  if (auto s = sessionById(id); s.has_value()) {
    mSessions.removeOne(s.value());
    s.value()->deleteLater();
    emit sessionClosed(id);
  }
}

int SessionManager::newSessionId() {
  return mSessions.isEmpty() ? 0 : mSessions.last()->id() + 1;
}

void SessionManager::connectionToConnectionManager(
    const Session *s, Connections::ConnectionManager *cm) {
  for (const auto &con : s->connections()) {
    cm->connectToConnection(s->id(), con);
  }
}

Utilities::Result<Session *, SessionError>
SessionManager::sessionById(const int id) const noexcept {
  for (auto s : mSessions) {
    if (s->id() == id) {
      return s;
    }
  }
  return Err{
      SessionError{ErrorType::SessionNotFound,
                   QStringLiteral("Session with Id %1 not found").arg(id)}};
}

} // namespace Sessions
