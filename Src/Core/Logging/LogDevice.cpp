#include "LogDevice.hpp"
#include "QtNetwork/qhostaddress.h"
#include "QtNetwork/qtcpsocket.h"
#include <QDateTime>
#include <QIODevice>
namespace Log {

Logger::Logger(QObject *parent) : QObject{parent} {}
void Logger::setEnabled(const bool v) { mIsEnabled = v; }
bool Logger::isEnabled() const noexcept { return mIsEnabled; }
void Logger::setDevice(QIODevice *device) {
  if (device->open(QIODevice::WriteOnly | QIODevice::Unbuffered |
                   QIODevice::Text | QIODevice::Append)) {
    mDevice.reset(device);
    mLog.setDevice(device);
  } else {
    mLastError = device->errorString();
    device->deleteLater();
  }
}
void Logger::setSocketDevice(const QString &ip, const int port, const int msc) {
  auto socket = new QTcpSocket{};
  socket->connectToHost(QHostAddress{ip}, port,
                        QIODevice::WriteOnly | QIODevice::Unbuffered |
                            QIODevice::Text | QIODevice::Append);
  if (socket->waitForConnected(msc)) {
    mDevice.reset(socket);
    mLog.setDevice(socket);
  } else {
    mLastError = socket->errorString();
    socket->deleteLater();
  }
}

QString Logger::lastError() const noexcept { return mLastError; }
} // namespace Log
