#ifndef LOG_LOGDEVICE_HPP
#define LOG_LOGDEVICE_HPP

#include <QDateTime>
#include <QIODevice>
#include <QObject>
#include <QScopedPointer>
#include <QTextStream>

class QTcpSocket;
namespace Log {
enum LogType { Info, Warn, Error };

class Logger : public QObject {
  Q_OBJECT
public:
  explicit Logger(QObject *parent = nullptr);

  void setEnabled(const bool v);
  bool isEnabled() const noexcept;
  void setDevice(QIODevice *device);
  void setSocketDevice(const QString &ip, const int port, const int msc = 3000);
  QTextStream &operator()(const LogType type = LogType::Info);
  template <typename T>
  void log(const T &msg, const LogType type = LogType::Info) {
    if (mIsEnabled && !mDevice.isNull()) {
      const auto currentTime =
          QDateTime::currentDateTime().toString(Qt::ISODate);
      switch (type) {
      case LogType::Info:
        mLog << QStringLiteral("(Info) %1 > ").arg(currentTime);
        break;
      case LogType::Warn:
        mLog << QStringLiteral("(Warn) %1 > ").arg(currentTime);
        break;
      case LogType::Error:
        mLog << QStringLiteral("(Error) %1 > ").arg(currentTime);
      }
      mLog << msg;
    }
  }
  QString lastError() const noexcept;
signals:

private:
  QTextStream mLog;
  QScopedPointer<QIODevice> mDevice;
  bool mIsEnabled = false;
  QString mLastError;
};

} // namespace Log

#endif // LOG_LOGDEVICE_HPP
