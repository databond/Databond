#ifndef CONTROL_CLIENTMANAGER_HPP
#define CONTROL_CLIENTMANAGER_HPP

#include "ClientError.hpp"
#include <QMap>
#include <QObject>
#include <QReadWriteLock>
#include <Utilities/expected.hpp>
namespace Control {
class Client;
class ClientManager : public QObject {
  Q_OBJECT
public:
  explicit ClientManager(QObject *parent = nullptr);

public:
  static Utilities::Result<Client *, ClientError> client(const QString &id);
  static Utilities::Result<QString, ClientError> request();
  static Utilities::Result<QString, ClientError> request(const QString &id);
  static void remove(const QString &id);
  static void setLimit(const int limit);
  static bool exist(const QString &id) noexcept;

private:
  inline static QMap<QString, Client *> mClients;
  inline static QReadWriteLock mMutex{QReadWriteLock::NonRecursive};
  inline static int mLimit = -1;
};

} // namespace Control

#endif // CONTROL_CLIENTMANAGER_HPP
