#include "ClientError.hpp"

namespace Control {

ClientError::ClientError() : Utilities::Error{} {}

ClientError::ClientError(const Utilities::ErrorType type, const QString &msg)
    : Utilities::Error{type, msg} {}

bool ClientError::isClientError() const noexcept { return true; }

QString ClientError::errorTag() const noexcept { return "ClientError"; }
} // namespace Control
