#include "Client.hpp"

using namespace Sessions;
using namespace Connections;
namespace Control {
Client::Client(QObject *parent)
    : Client{QUuid::createUuid().toString(QUuid::WithoutBraces), parent} {}

Client::Client(const QString &id, QObject *parent)
    : mId{id}, mSession{mId, this}, mConnection{mId, &mSession, this} {}

QString Client::id() const noexcept { return mId; }

} // namespace Control
