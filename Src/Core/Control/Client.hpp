#ifndef SESSIONS_CLIENT_HPP
#define SESSIONS_CLIENT_HPP

#include "ClientError.hpp"
#include <Connections/ConnectionManager.hpp>

#include <Logging/LogDevice.hpp>
#include <QFile>
#include <QObject>
#include <QThreadPool>
#include <QUuid>
#include <Sessions/Session.hpp>
#include <Sessions/SessionError.hpp>
#include <Sessions/SessionManager.hpp>
#include <Utilities/expected.hpp>

template <typename Class> struct MemberFunctionTraits {};

template <typename Ret, typename Class, typename... Args>
struct MemberFunctionTraits<Ret (Class::*)(Args...)> {
  using ClassType = Class;
  using RetType = Ret;
  using FuncType = Ret (Class::*)(Args...);
};

template <typename Ret, typename Class, typename... Args>
struct MemberFunctionTraits<Ret (Class::*)(Args...) const> {
  using ClassType = Class;
  using RetType = Ret;
  using FuncType = Ret (Class::*)(Args...) const;
};

template <typename Ret, typename Class, typename... Args>
struct MemberFunctionTraits<Ret (Class::*)(Args...) noexcept> {
  using ClassType = Class;
  using RetType = Ret;
  using FuncType = Ret (Class::*)(Args...) noexcept;
};

template <typename Ret, typename Class, typename... Args>
struct MemberFunctionTraits<Ret (Class::*)(Args...) const noexcept> {
  using ClassType = Class;
  using RetType = Ret;
  using FuncType = Ret (Class::*)(Args...) const noexcept;
};

namespace Connections {
class ConnectionManager;
}

namespace Control {
class Client : public QObject {
  Q_OBJECT
public:
  explicit Client(QObject *parent = nullptr);
  explicit Client(const QString &id, QObject *parent = nullptr);
  QString id() const noexcept;

  template <typename Func, typename... Args>
  typename MemberFunctionTraits<Func>::RetType call(Func func,
                                                    const Args &...args) {
    using M = typename MemberFunctionTraits<Func>::ClassType;
    using T = typename MemberFunctionTraits<Func>::RetType;
    using namespace Sessions;
    using namespace Connections;
    static_assert(std::is_same_v<M, SessionManager> ||
                      std::is_same_v<M, ConnectionManager> ||
                      std::is_same_v<M, Log::Logger>,
                  "The type must be either for SessionManager, "
                  "ConnectionManager or LogDevice");
    QMutexLocker locker{&mMutex};
    if constexpr (std::is_void_v<T>) {
      if constexpr (std::is_same_v<M, SessionManager>) {
        (mSession.*func)(args...);
      } else if constexpr (std::is_same_v<M, Log::Logger>) {
        (mLogger.*func)(args...);
      } else {
        (mConnection.*func)(args...);
      }
    } else {
      if constexpr (std::is_same_v<M, SessionManager>) {
        return (mSession.*func)(args...);
      } else if constexpr (std::is_same_v<M, Log::Logger>) {
        return (mLogger.*func)(args...);
      } else {
        return (mConnection.*func)(args...);
      }
    }
  }

private:
  const QString mId;
  QMutex mMutex;

public:
  Sessions::SessionManager mSession;
  Connections::ConnectionManager mConnection;
  Log::Logger mLogger;
};

} // namespace Control

#endif // SESSIONS_CLIENT_HPP
