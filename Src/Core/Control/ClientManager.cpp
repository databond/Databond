#include "ClientManager.hpp"
#include "Client.hpp"
#include <QReadLocker>
#include <QWriteLocker>

namespace Control {

ClientManager::ClientManager(QObject *parent) : QObject(parent) {}

Utilities::Result<Client *, ClientError>
ClientManager::client(const QString &id) {
  QReadLocker locker{&mMutex};
  if (mClients.contains(id)) {
    return mClients.value(id);
  }
  return Utilities::Err{
      ClientError{Utilities::ClientNotFound,
                  QStringLiteral("The client with Id %1 not found").arg(id)}};
}

Utilities::Result<QString, ClientError> ClientManager::request() {
  QWriteLocker locker{&mMutex};
  if (mLimit < 0 || mClients.size() < mLimit) {
    auto c = new Client{QUuid::createUuid().toString(QUuid::WithoutBraces)};
    const auto id = c->id();
    mClients.insert(id, c);
    return id;
  } else {
    return Utilities::Err{ClientError{
        Utilities::LimitError,
        QStringLiteral("Can't allocate more clients due to the limit "
                       "constraints, only %1 clients are allowed")
            .arg(mLimit)}};
  }
}

Utilities::Result<QString, ClientError>
ClientManager::request(const QString &id) {
  QWriteLocker locker{&mMutex};
  if (mClients.contains(id)) {
    return Utilities::Err{ClientError{Utilities::CLIENT_ERROR,
                                      QStringLiteral("Client already exist")}};
  }
  if ((mLimit < 0 || mClients.size() < mLimit)) {
    auto c = new Client{id};
    const auto id = c->id();
    mClients.insert(id, c);
    return id;
  } else {
    return Utilities::Err{ClientError{
        Utilities::LimitError,
        QStringLiteral("Can't allocate more clients due to the limit "
                       "constraints, only %1 clients are allowed")
            .arg(mLimit)}};
  }
}

void ClientManager::remove(const QString &id) {
  QWriteLocker locker{&mMutex};
  if (mClients.contains(id)) {
    auto client = mClients[id];
    delete client;
    mClients.remove(id);
  }
}

void ClientManager::setLimit(const int limit) { mLimit = limit; }

bool ClientManager::exist(const QString &id) noexcept {
  QReadLocker locker{&mMutex};
  return mClients.contains(id);
}

} // namespace Control
