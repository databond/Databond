#ifndef SESSIONS_CLIENTERROR_HPP
#define SESSIONS_CLIENTERROR_HPP

#include <Utilities/Error.hpp>

namespace Control {

class ClientError : public Utilities::Error {
public:
  ClientError();
  ClientError(const Utilities::ErrorType type, const QString &msg);

  // Error interface
public:
  virtual bool isClientError() const noexcept override;
  virtual QString errorTag() const noexcept override;
};

} // namespace Control
Q_DECLARE_METATYPE(Control::ClientError)
#endif // SESSIONS_CLIENTERROR_HPP
