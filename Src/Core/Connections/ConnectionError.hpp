#ifndef CONNECTIONS_CONNECTIONERROR_HPP
#define CONNECTIONS_CONNECTIONERROR_HPP

#include <QObject>
#include <QString>
#include <Utilities/Error.hpp>

namespace Connections {
class ConnectionError : public Utilities::Error {
public:
  ConnectionError();
  ConnectionError(const Utilities::ErrorType type, const QString &msg);
  ConnectionError(const ConnectionError &);
  ConnectionError(ConnectionError &&);

public:
  auto operator=(const ConnectionError &) -> ConnectionError &;
  auto operator=(ConnectionError &&) -> ConnectionError &;
  // Error interface
public:
  virtual QString explain() const noexcept override;

  // Error interface
public:
  virtual bool isConnectionError() const noexcept override;

  virtual QString errorTag() const noexcept override;
};

} // namespace Connections
Q_DECLARE_METATYPE(Connections::ConnectionError)
#endif // CONNECTIONS_CONNECTIONEERROR_HPP
