#include "ConnectionManager.hpp"
#include "Connection.hpp"
#include <Control/Client.hpp>
#include <Sessions/Session.hpp>
#include <Utilities/Namer.hpp>

namespace Connections {
using namespace Utilities;
using namespace Sessions;
using namespace Control;

ConnectionManager::ConnectionManager(const QString &clientId,
                                     SessionManager *sessionManager,
                                     QObject *parent)
    : QObject{parent}, mClientId{clientId}, mSessionManager(sessionManager) {}

void ConnectionManager::close(const int id, const QString &conName) {
  connection<void>(id, conName, &Connection::close);
}

UnitType ConnectionManager::commit(const int id, const QString &conName) {
  return connection<Unit>(id, conName, &Connection::commit);
}

PureType<QString> ConnectionManager::connectionOptions(const int id,
                                                       const QString &conName) {
  return connection<QString>(id, conName, &Connection::connectionOptions);
}

PureType<QString> ConnectionManager::connectionName(const int id,
                                                    const QString &conName) {
  return connection<QString>(id, conName, &Connection::connectionName);
}

PureType<QString> ConnectionManager::databaseName(const int id,
                                                  const QString &conName) {
  return connection<QString>(id, conName, &Connection::databaseName);
}

SessionConnectionType<QVariant> ConnectionManager::exec(const int id,
                                                        const QString &conName,
                                                        const QString &query) {
  return connection<QVariant>(id, conName, &Connection::exec, query);
}

PureType<QString> ConnectionManager::hostName(const int id,
                                              const QString &conName) {
  return connection<QString>(id, conName, &Connection::hostName);
}

PureType<bool> ConnectionManager::isOpen(const int id, const QString &conName) {
  return connection<bool>(id, conName, &Connection::isOpen);
}

PureType<bool> ConnectionManager::isOpenError(const int id,
                                              const QString &conName) {

  return connection<bool>(id, conName, &Connection::isOpenError);
}

PureType<QSqlError> ConnectionManager::lastSqlError(const int id,
                                                    const QString &conName) {
  return connection<QSqlError>(id, conName, &Connection::lastSqlError);
}

UnitType ConnectionManager::open(const int id, const QString &conName) {
  return connection<Unit>(id, conName, QOverload<>::of(&Connection::open));
}

UnitType ConnectionManager::open(const int id, const QString &conName,
                                 const QString &username,
                                 const QString &password) {
  return connection<Unit>(
      id, conName,
      QOverload<const QString &, const QString &>::of(&Connection::open),
      username, password);
}

PureType<QString> ConnectionManager::password(const int id,
                                              const QString &conName) {
  return connection<QString>(id, conName, &Connection::password);
}

PureType<int> ConnectionManager::port(const int id, const QString &conName) {
  return connection<int>(id, conName, &Connection::port);
}
PureType<QString> ConnectionManager::plugin(const int id,
                                            const QString &conName) {
  return connection<QString>(id, conName, &Connection::plugin);
}

SessionConnectionType<QSqlIndex>
ConnectionManager::primaryIndex(const int id, const QString &conName,
                                const QString &tableName) {
  return connection<QSqlIndex>(id, conName, &Connection::primaryIndex,
                               tableName);
}

SessionConnectionType<QSqlRecord>
ConnectionManager::record(const int id, const QString &conName,
                          const QString &tableName) {
  return connection<QSqlRecord>(id, conName, &Connection::record, tableName);
}

UnitType ConnectionManager::rollback(const int id, const QString &conName) {
  return connection<Unit>(id, conName, &Connection::rollback);
}

void ConnectionManager::setConnectionOptions(const int id,
                                             const QString &conName,
                                             const QString &options) {
  connection<void>(id, conName, &Connection::setConnectionOptions, options);
}

void ConnectionManager::setDatabaseName(const int id, const QString &conName,
                                        const QString &name) {
  connection<void>(id, conName, &Connection::setDatabaseName, name);
}

void ConnectionManager::setHostName(const int id, const QString &conName,
                                    const QString &hostName) {
  connection<void>(id, conName, &Connection::setHostName, hostName);
}

void ConnectionManager::setPassword(const int id, const QString &conName,
                                    const QString &password) {
  connection<void>(id, conName, &Connection::setPassword, password);
}

void ConnectionManager::setPort(const int id, const QString &conName,
                                const int port) {
  connection<void>(id, conName, &Connection::setPort, port);
}

void ConnectionManager::setUsername(const int id, const QString &conName,
                                    const QString &username) {
  connection<void>(id, conName, &Connection::setUsername, username);
}

SessionConnectionType<QStringList>
ConnectionManager::tables(const int id, const QString &conName,
                          const QString &tableType) {
  return connection<QStringList>(id, conName, &Connection::tables, tableType);
}

UnitType ConnectionManager::transaction(const int id, const QString &conName) {
  return connection<Unit>(id, conName, &Connection::transaction);
}

PureType<QString> ConnectionManager::username(const int id,
                                              const QString &conName) {
  return connection<QString>(id, conName, &Connection::username);
}

SessionConnectionType<QStringList>
ConnectionManager::columns(const int id, const QString &conName,
                           const QString &tableName) {
  return connection<QStringList>(id, conName, &Connection::columns, tableName);
}

SessionConnectionType<QList<QPair<QString, QString>>>
ConnectionManager::columnsWithTypes(const int id, const QString &conName,
                                    const QString &tableName) {
  return connection<QList<QPair<QString, QString>>>(
      id, conName, &Connection::columnsWithTypes, tableName);
}

PureType<QStringList> ConnectionManager::types(const int id,
                                               const QString &conName) {
  return connection<QStringList>(id, conName, &Connection::types);
}

PureType<QStringList>
ConnectionManager::availableOptions(const int id, const QString &conName) {
  return connection<QStringList>(id, conName, &Connection::availableOptions);
}

SessionConnectionType<QStringList>
ConnectionManager::triggers(const int id, const QString &conName) {
  return connection<QStringList>(id, conName, &Connection::triggers);
}

PureType<ConnectionError> ConnectionManager::lastError(const int id,
                                                       const QString &conName) {
  return connection<ConnectionError>(id, conName,
                                     &Connection::lastConnectionError);
}
QString ConnectionManager::clientId() const noexcept { return mClientId; }
SessionConnectionType<QList<QPair<QString, QString>>>
ConnectionManager::foreignKeys(const int id, const QString &conName,
                               const QString &tableName) {
  return connection<QList<QPair<QString, QString>>>(
      id, conName, &Connection::foreignKeys, tableName);
}

void ConnectionManager::addConnection(const int id, const QString &pluginName,
                                      const QString &conName) {
  if (mSessionManager.isNull() || exist(id, conName)) {
    return;
  }
  Namer::makeUniqueConnectionName(mClientId, id, conName)
      .map([this, id, conName, pluginName](const QString &fullName) {
        mSessionManager->sessionById(id).map(
            [this, id, pluginName, conName, fullName](Session *s) {
              s->addConnection(pluginName, fullName)
                  .map([s, this](Connection *con) {
                    connectToConnection(s->id(), con);
                  });
            });
      });
}
QStringList ConnectionManager::connectionNames(const int sessionId) const {
  if (mSessionManager.isNull() &&
      !mSessionManager->sessionById(sessionId).has_value()) {
    return {};
  }
  return mSessionManager->sessionById(sessionId).value()->connectionNames();
}

void ConnectionManager::addConnection(const int id, const QJsonObject &obj) {
  if (mSessionManager.isNull() ||
      exist(id, obj["ConnectionName"].toString(""))) {
    return;
  }
  Namer::makeUniqueConnectionName(mClientId, id,
                                  obj["ConnectionName"].toString())
      .map([this, id, obj](const QString &fullName) {
        auto conObj = obj;
        const auto resolvedName = conObj["ConnectionName"].toString();
        conObj["ConnectionName"] = fullName;
        mSessionManager->sessionById(id).map(
            [this, id, conObj, resolvedName](Session *s) {
              s->addConnection(conObj).map([s, this](Connection *con) {
                connectToConnection(s->id(), con);
              });
            });
      });
}

void ConnectionManager::removeConnection(const int id, const QString &conName) {
  if (mSessionManager.isNull()) {
    return;
  }
  Namer::makeUniqueConnectionName(mClientId, id, conName)
      .map([this, id](const QString &fullName) {
        mSessionManager->sessionById(id).map(
            [fullName](Session *s) { s->removeConnection(fullName); });
      });
}

Result<int, SessionError> ConnectionManager::count(const int id) {
  if (mSessionManager.isNull()) {
    return Err{SessionError{
        ErrorType::SESSION_ERROR,
        QStringLiteral("No session manager was set to to client %1")
            .arg(mClientId)}};
  }
  return mSessionManager->sessionById(id).map(
      [](Session *s) { return s->connectionsCount(); });
}

bool ConnectionManager::exist(const int id, const QString &conName) {
  if (!mSessionManager.isNull()) {
    if (auto s = mSessionManager->sessionById(id); s.has_value()) {
      return s.value()
          ->connectionByName(
              QStringLiteral("%1.%2.%3").arg(mClientId).arg(id).arg(conName))
          .has_value();
    }
  }
  return false;
}

void ConnectionManager::connectToConnection(const int id, Connection *con) {
  connect(con, &Connection::connectionOpened, this,
          [this, id](const QString &conName) {
            Namer::getConnectionNamePart(conName).map(
                [this, id](const QString &conName) {
                  emit connectionOpened(id, conName);
                });
          });
  connect(con, &Connection::connectionClosed, this,
          [this, id](const QString &conName) {
            Namer::getConnectionNamePart(conName).map(
                [this, id](const QString &conName) {
                  emit connectionClosed(id, conName);
                });
          });
  connect(con, &Connection::connectionRemoved, this, [this, id](QString name) {
    Namer::getConnectionNamePart(name).map([this, id](const QString &conName) {
      emit connectionRemoved(id, conName);
    });
  });
  connect(con, &Connection::connectionTransactionBegan, this,
          [this, id](const QString &conName) {
            Namer::getConnectionNamePart(conName).map(
                [this, id](const QString &conName) {
                  emit connectionTransactionBegan(id, conName);
                });
          });
  connect(con, &Connection::connectionRollback, this,
          [this, id](const QString &conName) {
            Namer::getConnectionNamePart(conName).map(
                [this, id](const QString &conName) {
                  emit connectionRollback(id, conName);
                });
          });
  connect(con, &Connection::connectionCommited, this,
          [this, id](const QString &conName) {
            Namer::getConnectionNamePart(conName).map(
                [this, id](const QString &conName) {
                  emit connectionCommited(id, conName);
                });
          });

  Namer::getConnectionNamePart(con->connectionName())
      .map([this, id](const QString &conName) {
        emit connectionRegistered(id, conName);
      });
}

} // namespace Connections
