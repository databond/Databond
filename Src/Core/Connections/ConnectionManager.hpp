#ifndef CONNECTIONS_CONNECTIONMANAGER_HPP
#define CONNECTIONS_CONNECTIONMANAGER_HPP

#include "ConnectionError.hpp"
#include <QFuture>
#include <QJsonObject>
#include <QObject>
#include <QPointer>
#include <QSqlError>
#include <QSqlIndex>
#include <QSqlRecord>
#include <Sessions/SessionManager.hpp>
#include <Utilities/Namer.hpp>
#include <Utilities/expected.hpp>

namespace Control {
class Client;
}
namespace Sessions {
class Session;
class SessionManager;
} // namespace Sessions
namespace Utilities {
class UtilitiesError;
}
namespace Connections {
class Connection;
class DBConnection;
class ConnectionError;

using UnitType = Utilities::Result<Utilities::Unit, Utilities::ErrorVariant>;

template <typename T>
using SessionConnectionType = Utilities::Result<T, Utilities::ErrorVariant>;

template <typename T>
using PureType = Utilities::Result<T, Utilities::ErrorVariant>;

class ConnectionManager : public QObject {
  Q_OBJECT
public:
  explicit ConnectionManager(const QString &clientId,
                             Sessions::SessionManager *sessionManager = nullptr,
                             QObject *parent = nullptr);

  void close(const int id, const QString &conName);
  UnitType commit(const int id, const QString &conName);
  PureType<QString> plugin(const int id, const QString &conName);
  PureType<QString> connectionOptions(const int id, const QString &conName);
  PureType<QString> connectionName(const int id, const QString &conName);
  PureType<QString> databaseName(const int id, const QString &conName);
  SessionConnectionType<QVariant> exec(const int id, const QString &conName,
                                       const QString &query);
  PureType<QString> hostName(const int id, const QString &conName);
  PureType<bool> isOpen(const int id, const QString &conName);
  PureType<bool> isOpenError(const int id, const QString &conName);
  PureType<QSqlError> lastSqlError(const int id, const QString &conName);
  UnitType open(const int id, const QString &conName);
  UnitType open(const int id, const QString &conName, const QString &username,
                const QString &password);
  PureType<QString> password(const int id, const QString &conName);
  PureType<int> port(const int id, const QString &conName);
  SessionConnectionType<QSqlIndex>
  primaryIndex(const int id, const QString &conName, const QString &tableName);
  SessionConnectionType<QSqlRecord> record(const int id, const QString &conName,
                                           const QString &tableName);
  UnitType rollback(const int id, const QString &conName);
  void setConnectionOptions(const int id, const QString &conName,
                            const QString &options);
  void setDatabaseName(const int id, const QString &conName,
                       const QString &name);
  void setHostName(const int id, const QString &conName,
                   const QString &hostName);
  void setPassword(const int id, const QString &conName,
                   const QString &password);
  void setPort(const int id, const QString &conName, const int port);
  void setUsername(const int id, const QString &conName,
                   const QString &username);
  SessionConnectionType<QStringList>
  tables(const int id, const QString &conName, const QString &tableType);
  UnitType transaction(const int id, const QString &conName);
  PureType<QString> username(const int id, const QString &conName);
  SessionConnectionType<QStringList>
  columns(const int id, const QString &conName, const QString &tableName);
  SessionConnectionType<QList<QPair<QString, QString>>>
  columnsWithTypes(const int id, const QString &conName,
                   const QString &tableName);
  PureType<QStringList> types(const int id, const QString &conName);
  PureType<QStringList> availableOptions(const int id, const QString &conName);
  SessionConnectionType<QStringList> triggers(const int id,
                                              const QString &conName);
  PureType<ConnectionError> lastError(const int id, const QString &conName);

  SessionConnectionType<QList<QPair<QString, QString>>>
  foreignKeys(const int id, const QString &conName, const QString &tableName);

  void addConnection(const int id, const QString &pluginName,
                     const QString &conName);
  void addConnection(const int id, const QJsonObject &obj);
  void removeConnection(const int id, const QString &conName);
  Utilities::Result<int, Sessions::SessionError> count(const int id);
  bool exist(const int id, const QString &conName);

  void connectToConnection(const int id, Connection *con);
  QString clientId() const noexcept;
  QStringList connectionNames(const int id) const;

signals:
  void connectionRegistered(int, QString);
  void connectionOpened(int, QString);
  void connectionClosed(int, QString);
  void connectionRemoved(int, QString);
  void connectionTransactionBegan(int, QString);
  void connectionRollback(int, QString);
  void connectionCommited(int, QString);

private:
  template <typename T, typename Func, typename... Args>
  Utilities::Result<std::conditional_t<std::is_void_v<T>, Utilities::Unit, T>,
                    Utilities::ErrorVariant>
  connection(const int id, const QString &conName, Func fun,
             const Args &...args);

private:
  QString mClientId;
  QPointer<Sessions::SessionManager> mSessionManager;
};

template <typename T, typename Func, typename... Args>
Utilities::Result<std::conditional_t<std::is_void_v<T>, Utilities::Unit, T>,
                  Utilities::ErrorVariant>
ConnectionManager::connection(const int id, const QString &conName, Func fun,
                              const Args &...args) {
  if (mSessionManager.isNull()) {
    return Utilities::Err{Sessions::SessionError{
        Utilities::ErrorType::SESSION_ERROR,
        QStringLiteral("No Session manager was set to client %1")
            .arg(mClientId)}};
  }
  const auto clientId = mClientId;
  const auto fullConName =
      Utilities::Namer::makeUniqueConnectionName(clientId, id, conName);
  if (!fullConName) {
    return Utilities::Err{fullConName.error()};
  }
  if (auto s = mSessionManager->sessionById(id); s) {
    if (auto con = s.value()->connectionByName(fullConName.value()); con) {
      if constexpr (std::is_void_v<T>) {
        (con.value()->*fun)(args...);
        return Utilities::Unit{};
      } else {
        return (con.value()->*fun)(args...);
      }
    } else {
      return Utilities::Err{con.error()};
    }
  } else {
    return Utilities::Err{s.error()};
  }
}

} // namespace Connections

#endif // CONNECTIONS_CONNECTIONMANAGER_HPP
