#ifndef CONNECTIONS_HPP
#define CONNECTIONS_HPP
#include "ConnectionError.hpp"
#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlIndex>
#include <QSqlRecord>
#include <Utilities/Json.hpp>
#include <Utilities/expected.hpp>

namespace Connections {
class Connection;
class DBConnection;
inline auto safeRemove(Connection *con) -> void;

class Connection : public QObject {
  Q_OBJECT
public:
  explicit Connection(QObject *parent = nullptr) : QObject{parent} {}

public:
  virtual auto plugin() -> QString { return ""; }
  virtual auto close() -> void = 0;
  virtual auto commit() -> Utilities::UnitResult<ConnectionError> = 0;
  virtual auto connectionOptions() -> QString = 0;
  virtual auto connectionName() -> QString = 0;
  virtual auto databaseName() -> QString = 0;
  virtual auto exec(const QString &query)
      -> Utilities::Result<QVariant, ConnectionError> = 0;
  virtual auto hostName() -> QString = 0;
  virtual auto isOpen() -> bool = 0;
  virtual auto isOpenError() -> bool = 0;
  virtual auto isValid() -> bool = 0;
  virtual auto lastSqlError() -> QSqlError = 0;
  virtual auto numericalPrecisionPolicy() -> QSql::NumericalPrecisionPolicy = 0;
  virtual auto open() -> Utilities::UnitResult<ConnectionError> = 0;
  virtual auto open(const QString &username, const QString &password)
      -> Utilities::UnitResult<ConnectionError> = 0;
  virtual auto password() -> QString = 0;
  virtual auto port() -> int = 0;
  virtual auto primaryIndex(const QString &tableName)
      -> Utilities::Result<QSqlIndex, ConnectionError> = 0;
  virtual auto record(const QString &tableName)
      -> Utilities::Result<QSqlRecord, ConnectionError> = 0;
  virtual auto rollback() -> Utilities::UnitResult<ConnectionError> = 0;
  virtual auto setConnectionOptions(const QString &option) -> void = 0;
  virtual auto setDatabaseName(const QString &name) -> void = 0;
  virtual auto setHostName(const QString &name) -> void = 0;
  virtual auto
  setNumericalPrecisionPolicy(QSql::NumericalPrecisionPolicy precisionPolicy)
      -> void = 0;
  virtual auto setPassword(const QString &password) -> void = 0;
  virtual auto setPort(const int port) -> void = 0;
  virtual auto setUsername(const QString &username) -> void = 0;
  virtual auto tables(const QString &type)
      -> Utilities::Result<QStringList, ConnectionError> = 0;
  virtual auto transaction() -> Utilities::UnitResult<ConnectionError> = 0;
  virtual auto username() -> QString = 0;
  virtual auto columns(const QString &tableName)
      -> Utilities::Result<QStringList, ConnectionError> = 0;
  virtual auto columnsWithTypes(const QString &tableName)
      -> Utilities::Result<QList<QPair<QString, QString>>, ConnectionError> = 0;
  virtual auto types() -> QStringList = 0;
  virtual auto dbms() -> QString = 0;
  virtual auto availableOptions() -> QStringList = 0;
  virtual auto triggers()
      -> Utilities::Result<QStringList, ConnectionError> = 0;

  virtual auto database() -> QSqlDatabase = 0;
  virtual auto foreignKeys(const QString &tableName)
      -> Utilities::Result<QList<QPair<QString, QString>>, ConnectionError> = 0;

  virtual auto toJson() -> QJsonObject = 0;
  auto lastConnectionError() -> ConnectionError { return mLastError; }

signals:
  void connectionOpened(QString);
  void connectionClosed(QString);
  void connectionRemoved(QString);
  void connectionTransactionBegan(QString);
  void connectionCommited(QString);
  void connectionRollback(QString);

protected:
  void setConnectionNotOpenedError() {
    mLastError.setError(
        Utilities::ErrorType::ConnectionNotOpened,
        QStringLiteral("%1 is not open").arg(database().connectionName()));
  }

protected:
  ConnectionError mLastError;
};

template <typename ConnectionType = DBConnection>
inline auto fromJson(const QJsonObject &obj)
    -> Utilities::Result<ConnectionType *, ConnectionError> {
  using namespace Utilities;
  if (!Utilities::Json::validateConnectionSchema(obj)) {
    return Err{ConnectionError{
        ErrorType::SourceError,
        QStringLiteral("Failed to create a connection from an invalid json "
                       "source, the source must look like this: %1")
            .arg(QString::fromUtf8(
                QJsonDocument(Utilities::Json::makeConnectionJsonTemplate())
                    .toJson(QJsonDocument::Indented)))}};
  }
  auto con = new ConnectionType{obj["PluginName"].toString(),
                                obj["ConnectionName"].toString()};
  if (const auto er = con->lastConnectionError();
      er == Utilities::ErrorType::PluginError) {
    safeRemove(con);
    return Utilities::Err{er};
  }
  con->setUsername(obj["Username"].toString());
  con->setPassword(obj["Password"].toString());
  con->setPort(obj["Port"].toInt());
  con->setHostName(obj["HostName"].toString());
  con->setConnectionOptions(obj["Options"].toString());
  con->setDatabaseName(obj["DatabaseName"].toString());
  return con;
}

inline auto safeRemove(Connection *con) -> void {
  if (!con) {
    return;
  }
  con->close();
  const auto name = con->connectionName();
  emit con->connectionRemoved(name);
  delete con;
  QSqlDatabase::removeDatabase(name);
}

} // namespace Connections
#endif // CONNECTIONS_HPP
