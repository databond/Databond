#ifndef CONNECTIONS_DBCONNECTIONS_HPP
#define CONNECTIONS_DBCONNECTIONS_HPP

#include "Connection.hpp"

#include <QPointer>
namespace Plugins {
class Plugin;
}

namespace Connections {

class DBConnection : public Connections::Connection, private QSqlDatabase {
  Q_OBJECT
public:
  explicit DBConnection(const QString &mPlugin, const QString &conName,
                        QObject *parent = nullptr);

  // Connection interface
public:
  virtual void close() override;
  virtual Utilities::UnitResult<ConnectionError> commit() override;
  virtual QString connectionOptions() override;
  virtual QString connectionName() override;
  virtual QString databaseName() override;
  virtual Utilities::Result<QVariant, ConnectionError>
  exec(const QString &query) override;
  virtual QString hostName() override;
  virtual bool isOpen() override;
  virtual bool isOpenError() override;
  virtual bool isValid() override;
  virtual QSqlError lastSqlError() override;
  virtual QSql::NumericalPrecisionPolicy numericalPrecisionPolicy() override;
  virtual Utilities::UnitResult<ConnectionError> open() override;
  virtual Utilities::UnitResult<ConnectionError>
  open(const QString &username, const QString &password) override;
  virtual QString password() override;
  virtual int port() override;
  virtual Utilities::Result<QSqlIndex, ConnectionError>
  primaryIndex(const QString &tableName) override;
  virtual Utilities::Result<QSqlRecord, ConnectionError>
  record(const QString &tableName) override;
  virtual Utilities::UnitResult<ConnectionError> rollback() override;
  virtual void setConnectionOptions(const QString &option) override;
  virtual void setDatabaseName(const QString &name) override;
  virtual void setHostName(const QString &name) override;
  virtual void setNumericalPrecisionPolicy(
      QSql::NumericalPrecisionPolicy precisionPolicy) override;
  virtual void setPassword(const QString &password) override;
  virtual void setPort(const int port) override;
  virtual void setUsername(const QString &username) override;
  virtual Utilities::Result<QStringList, ConnectionError>
  tables(const QString &type) override;
  virtual Utilities::UnitResult<ConnectionError> transaction() override;
  virtual QString username() override;
  virtual Utilities::Result<QStringList, ConnectionError>
  columns(const QString &tableName) override;
  virtual Utilities::Result<QList<QPair<QString, QString>>, ConnectionError>
  columnsWithTypes(const QString &tableName) override;
  virtual QStringList types() override;
  virtual QString dbms() override;
  virtual QStringList availableOptions() override;
  virtual Utilities::Result<QStringList, ConnectionError> triggers() override;
  virtual QSqlDatabase database() override;
  virtual Utilities::Result<QList<QPair<QString, QString>>, ConnectionError>
  foreignKeys(const QString &tableName) override;
  virtual QJsonObject toJson() override;
  virtual auto plugin() -> QString override;

private:
  QString mName, mPluginName;
  QPointer<Plugins::Plugin> mHandle;
};

} // namespace Connections

#endif // CONNECTIONS_DBCONNECTIONS_HPP
