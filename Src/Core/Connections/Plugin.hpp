#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <QObject>
#include <QSqlDriver>
#include <QtGlobal>
namespace Plugins {

class Plugin : public QSqlDriver {
  Q_OBJECT
protected:
  explicit Plugin(QSqlDriverPrivate &dp, QObject *parent = nullptr)
      : QSqlDriver{dp, parent} {}

public:
  explicit Plugin(QObject *parent = nullptr) : QSqlDriver{parent} {}
  Q_DISABLE_COPY(Plugin);
  virtual ~Plugin() {}

  // QSqlDriver interface
public:
  virtual QStringList columns(QSql::TableType,
                              const QString &entityName) const = 0;
  virtual QList<QPair<QString, QString>>
  columnWithType(QSql::TableType, const QString &entityName) const = 0;
  virtual QStringList types() const = 0;
  virtual QString dbms() const = 0;
  virtual bool isWithNetwork() const = 0;
  virtual QStringList availableOptions() const noexcept = 0;
  virtual QStringList triggers() const noexcept = 0;
  virtual QList<QPair<QString, QString>>
  foreignKeys(const QString &table) const noexcept = 0;
};
} // namespace Plugins
Q_DECLARE_INTERFACE(Plugins::Plugin, "note.bond.databond/plugin")

#endif // PLUGIN_HPP
