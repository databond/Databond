#include "ConnectionError.hpp"
#include <Utilities/Namer.hpp>

namespace Connections {

ConnectionError::ConnectionError() = default;

ConnectionError::ConnectionError(const Utilities::ErrorType type,
                                 const QString &msg)
    : Utilities::Error{type, msg} {}

ConnectionError &ConnectionError::operator=(ConnectionError &&) = default;

ConnectionError &ConnectionError::operator=(const ConnectionError &) = default;

ConnectionError::ConnectionError(const ConnectionError &) = default;

ConnectionError::ConnectionError(ConnectionError &&) = default;

QString ConnectionError::explain() const noexcept {
  return QStringLiteral("Connection Error: %1").arg(Error::explain());
}

} // namespace Connections

bool Connections::ConnectionError::isConnectionError() const noexcept {
  return true;
}

QString Connections::ConnectionError::errorTag() const noexcept {
  return "ConnectionError";
}
Utilities::ErrorVariant::ErrorVariant(const Connections::ConnectionError &er)
    : QVariant{QVariant::fromValue(er)} {}
auto Utilities::ErrorVariant::operator=(
    const Connections::ConnectionError &er) noexcept -> ErrorVariant & {
  setValue(fromValue<Connections::ConnectionError>(er));
  return *this;
}
bool Utilities::ErrorVariant::isConnectionError() const noexcept {
  return canConvert<Connections::ConnectionError>();
}
