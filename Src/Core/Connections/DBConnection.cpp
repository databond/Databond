#include "DBConnection.hpp"
#include "Plugin.hpp"
#include <QJsonObject>
#include <QSqlQuery>
namespace Connections {
using namespace Utilities;

DBConnection::DBConnection(const QString &pluginName, const QString &conName,
                           QObject *parent)
    : Connections::Connection(parent), QSqlDatabase{QSqlDatabase::addDatabase(
                                           pluginName, conName)},
      mName{conName}, mPluginName{pluginName} {
  mHandle = static_cast<Plugins::Plugin *>(QSqlDatabase::driver());
  if (!QSqlDatabase::isValid()) {
    mLastError.setError(ErrorType::PluginError,
                        QStringLiteral("Could not load plugin, %1")
                            .arg(QSqlDatabase::lastError().text()));
  }
}

} // namespace Connections

void Connections::DBConnection::close() {
  if (isOpen()) {
    QSqlDatabase::close();
    emit connectionClosed(QSqlDatabase::connectionName());
  }
}

Utilities::UnitResult<Connections::ConnectionError>
Connections::DBConnection::commit() {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Err{mLastError};
  }
  const auto c = QSqlDatabase::commit();
  if (c) {
    emit connectionCommited(connectionName());
    return Unit{};
  } else {
    mLastError.setError(ErrorType::OperationFailed, lastSqlError().text());
  }
  return Err{mLastError};
}

QString Connections::DBConnection::connectionOptions() {
  return QSqlDatabase::connectOptions();
}

QString Connections::DBConnection::connectionName() {
  return QSqlDatabase::connectionName();
}

QString Connections::DBConnection::databaseName() {
  return QSqlDatabase::databaseName();
}

Utilities::Result<QVariant, Connections::ConnectionError>
Connections::DBConnection::exec(const QString &query) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  QSqlQuery q{*this};
  if (q.exec(query)) {
    QJsonObject queryObj{{"Query", query}};
    if (q.isSelect()) {
      /**
       * {
       *  "Query": "query"
       *  "Result": {
       *      "Column1": [
       *
       *      ],
       *      "Column2": [
       *
       *      ]
       *  }
       * }
       *
       */
      QMap<QString, QJsonArray> columns;
      QJsonObject subResultObj;
      while (q.next()) {
        for (auto i = 0, count = q.record().count(); i < count; ++i) {
          const auto columnName = q.record().fieldName(i);
          const auto value = q.record().value(i);
          if (!columns.contains(columnName)) {
            columns[columnName] = {};
          }
          columns[columnName].push_back(value.toJsonValue());
        }
      }
      for (const auto &[key, v] : columns.toStdMap()) {
        subResultObj[key] = v;
      }
      queryObj["Result"] = subResultObj;
    } else {
      /**
       * {
       *  "Query" : "query",
       *  "Result" : true
       * }
       *
       */
      queryObj.insert("Result", true);
    }
    return queryObj;
  }
  mLastError.setError(ErrorType::OperationFailed, lastSqlError().text());
  return Err{mLastError};
}

QString Connections::DBConnection::hostName() {
  return QSqlDatabase::hostName();
}

bool Connections::DBConnection::isOpen() { return QSqlDatabase::isOpen(); }

bool Connections::DBConnection::isOpenError() {
  return QSqlDatabase::isOpenError();
}

bool Connections::DBConnection::isValid() { return QSqlDatabase::isValid(); }

QSqlError Connections::DBConnection::lastSqlError() {
  return QSqlDatabase::lastError();
}

QSql::NumericalPrecisionPolicy
Connections::DBConnection::numericalPrecisionPolicy() {
  return QSqlDatabase::numericalPrecisionPolicy();
}

Utilities::UnitResult<Connections::ConnectionError>
Connections::DBConnection::open() {
  const auto o = QSqlDatabase::open();
  if (o) {
    emit connectionOpened(connectionName());
    return Unit{};
  }
  setConnectionNotOpenedError();
  return Err{mLastError};
}

Utilities::UnitResult<Connections::ConnectionError>
Connections::DBConnection::open(const QString &username,
                                const QString &password) {

  const auto o = QSqlDatabase::open(username, password);
  if (o) {
    emit connectionOpened(connectionName());
    return Unit{};
  }
  setConnectionNotOpenedError();
  return Err{mLastError};
}

QString Connections::DBConnection::password() {
  return QSqlDatabase::password();
}

int Connections::DBConnection::port() { return QSqlDatabase::port(); }

Utilities::Result<QSqlIndex, Connections::ConnectionError>
Connections::DBConnection::primaryIndex(const QString &tableName) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return QSqlDatabase::primaryIndex(tableName);
}

Utilities::Result<QSqlRecord, Connections::ConnectionError>
Connections::DBConnection::record(const QString &tableName) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return QSqlDatabase::record(tableName);
}

Utilities::UnitResult<Connections::ConnectionError>
Connections::DBConnection::rollback() {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  const auto r = QSqlDatabase::rollback();
  if (r) {
    emit connectionRollback(connectionName());
    return Unit{};
  }
  mLastError.setError(ErrorType::OperationFailed, lastSqlError().text());
  return Err{mLastError};
}

void Connections::DBConnection::setConnectionOptions(const QString &option) {
  QSqlDatabase::setConnectOptions(option);
}

void Connections::DBConnection::setDatabaseName(const QString &name) {
  QSqlDatabase::setDatabaseName(name);
}

void Connections::DBConnection::setHostName(const QString &name) {
  QSqlDatabase::setHostName(name);
}

void Connections::DBConnection::setNumericalPrecisionPolicy(
    QSql::NumericalPrecisionPolicy precisionPolicy) {
  QSqlDatabase::setNumericalPrecisionPolicy(precisionPolicy);
}

void Connections::DBConnection::setPassword(const QString &password) {
  QSqlDatabase::setPassword(password);
}

void Connections::DBConnection::setPort(const int port) {
  QSqlDatabase::setPort(port);
}

void Connections::DBConnection::setUsername(const QString &username) {
  QSqlDatabase::setUserName(username);
}

Utilities::Result<QStringList, Connections::ConnectionError>
Connections::DBConnection::tables(const QString &type) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  QSql::TableType tableType = QSql::AllTables;
  const auto toLowerName = type.toLower();
  if (toLowerName == "tables") {
    tableType = QSql::Tables;
  } else if (toLowerName == "views") {
    tableType = QSql::Views;
  } else {
    tableType = QSql::AllTables;
  }
  return QSqlDatabase::tables(tableType);
}

Utilities::UnitResult<Connections::ConnectionError>
Connections::DBConnection::transaction() {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  const auto t = QSqlDatabase::transaction();
  if (t) {
    emit connectionTransactionBegan(connectionName());
    return Unit{};
  }
  mLastError.setError(ErrorType::OperationFailed, lastSqlError().text());
  return Err{mLastError};
}

QString Connections::DBConnection::username() {
  return QSqlDatabase::userName();
}

Utilities::Result<QStringList, Connections::ConnectionError>
Connections::DBConnection::columns(const QString &tableName) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return mHandle->columns(QSql::AllTables, tableName);
}

Utilities::Result<QList<QPair<QString, QString>>, Connections::ConnectionError>
Connections::DBConnection::columnsWithTypes(const QString &tableName) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return mHandle->columnWithType(QSql::AllTables, tableName);
}

QStringList Connections::DBConnection::types() { return mHandle->types(); }

QString Connections::DBConnection::dbms() { return mHandle->dbms(); }

QStringList Connections::DBConnection::availableOptions() {
  return mHandle->availableOptions();
}

Utilities::Result<QStringList, Connections::ConnectionError>
Connections::DBConnection::triggers() {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return mHandle->triggers();
}

QSqlDatabase Connections::DBConnection::database() { return *this; }

Utilities::Result<QList<QPair<QString, QString>>, Connections::ConnectionError>
Connections::DBConnection::foreignKeys(const QString &tableName) {
  if (!isOpen()) {
    setConnectionNotOpenedError();
    return Utilities::Err{mLastError};
  }
  return mHandle->foreignKeys(tableName);
}
QString Connections::DBConnection::plugin() {
  return QSqlDatabase::driverName();
}

QJsonObject Connections::DBConnection::toJson() {
  return {{"ConnectionName", connectionName()},
          {"DatabaseName", databaseName()},
          {"HostName", hostName()},
          {"Options", connectOptions()},
          {"Password", password()},
          {"PluginName", QSqlDatabase::driverName()},
          {"Port", port()},
          {"Username", username()}};
}
