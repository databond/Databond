#include "Error.hpp"
#include <Connections/ConnectionError.hpp>
#include <QMetaEnum>
#include <Sessions/SessionError.hpp>
#include <Utilities/UtilitiesError.hpp>

namespace Utilities {
Error::Error() : Error{ErrorType::NoError, ""} {}

Error::Error(const ErrorType type, const QString &msg)
    : mErrorString{msg}, mType{type} {}

Error::Error(const Error &) = default;

Error::Error(Error &&) = default;

QString Error::explain() const noexcept {
  return QStringLiteral("%1 -> %2")
      .arg(QMetaEnum::fromType<ErrorType>().valueToKey(mType))
      .arg(mErrorString);
}

bool Error::isConnectionError() const noexcept { return false; }

bool Error::isUtilityError() const noexcept { return false; }

bool Error::isSessionError() const noexcept { return false; }

bool Error::isClientError() const noexcept { return false; }

QString Error::errorTag() const noexcept { return "UnknowError"; }

bool Error::operator==(const ErrorType type) const noexcept {
  return mType == type;
}

bool Error::operator!=(const ErrorType type) const noexcept {
  return mType != type;
}

Error &Error::operator=(Error &&) = default;

Error &Error::operator=(const Error &) = default;

void Error::setError(const ErrorType type, const QString &msg) {
  mType = type;
  mErrorString = msg;
}

ErrorType Error::type() const noexcept { return mType; }
bool ErrorVariant::operator==(const ErrorType type) const noexcept {

  if (canConvert<Connections::ConnectionError>()) {
    return value<Connections::ConnectionError>().type() == type;
  } else if (canConvert<Sessions::SessionError>()) {
    return value<Sessions::SessionError>().type() == type;
  } else if (canConvert<UtilitiesError>()) {
    return value<UtilitiesError>().type() == type;
  } else {
    return false;
  }
}

bool ErrorVariant::operator!=(const ErrorType type) const noexcept {
  return !(*this == type);
}

} // namespace Utilities
