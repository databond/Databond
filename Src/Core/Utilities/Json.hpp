#ifndef JSON_HPP
#define JSON_HPP
#include <QFile>

#include <QtJsonSchema>

namespace Utilities::Json {

inline auto validateSessionSchema(const QJsonObject &obj) noexcept -> bool {
  QFile file{
      QStringLiteral(":/Json/ValdidateSession/SessionJsonValidation.json")};
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  const auto sc = JsonSchema::fromJsonString(file.readAll());
  return sc.validate(obj.toVariantMap());
}
inline auto validateConnectionSchema(const QJsonObject &obj) noexcept -> bool {
  QFile file{
      QStringLiteral(":/Json/ValdidateSession/ConnectionJsonValidation.json")};
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  const auto sc = JsonSchema::fromJsonString(file.readAll());
  return sc.validate(obj.toVariantMap());
}
inline auto makeConnectionJsonTemplate() noexcept -> QJsonObject {
  return {{"ConnectionName", ""},
          {"DatabaseName", ""},
          {"HostName", ""},
          {"Options", ""},
          {"Password", ""},
          {"PluginName", ""},
          {"Port", 0},
          {"Username", ""}};
}
inline auto makeSessionJsonTemplate() noexcept -> QJsonObject {
  return {{"Name", ""}, {"Connections", QJsonArray{}}};
}
} // namespace Utilities::Json
#endif // JSON_HPP
