#ifndef UTILITIES_UTILITIESEXCEPTION_HPP
#define UTILITIES_UTILITIESEXCEPTION_HPP

#include "Error.hpp"
#include <QString>

namespace Utilities {

class UtilitiesError : public Error {

public:
  UtilitiesError();
  UtilitiesError(const ErrorType type, const QString &msg);
  UtilitiesError(const UtilitiesError &);
  UtilitiesError(UtilitiesError &&);

public:
  auto operator=(const UtilitiesError &) -> UtilitiesError &;
  auto operator=(UtilitiesError &&) -> UtilitiesError &;
  // Error interface
public:
  virtual bool isUtilityError() const noexcept override;
  virtual QString errorTag() const noexcept override;
};

} // namespace Utilities
Q_DECLARE_METATYPE(Utilities::UtilitiesError)
#endif // UTILITIES_UTILITIESEXCEPTION_HPP
