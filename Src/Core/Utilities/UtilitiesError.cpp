#include "UtilitiesError.hpp"
#include <Connections/ConnectionError.hpp>
#include <Sessions/SessionError.hpp>

namespace Utilities {

UtilitiesError::UtilitiesError() = default;

UtilitiesError::UtilitiesError(const ErrorType type, const QString &msg)
    : Error{type, msg} {}

UtilitiesError &UtilitiesError::operator=(UtilitiesError &&) = default;
UtilitiesError &UtilitiesError::operator=(const UtilitiesError &) = default;

UtilitiesError::UtilitiesError(UtilitiesError &&) = default;

UtilitiesError::UtilitiesError(const UtilitiesError &) = default;

} // namespace Utilities

bool Utilities::UtilitiesError::isUtilityError() const noexcept { return true; }

QString Utilities::UtilitiesError::errorTag() const noexcept {
  return "UtilityError";
}
Utilities::ErrorVariant::ErrorVariant(const UtilitiesError &er)
    : QVariant{QVariant::fromValue(er)} {}
auto Utilities::ErrorVariant::operator=(const UtilitiesError &er) noexcept
    -> ErrorVariant & {
  setValue(fromValue<UtilitiesError>(er));
  return *this;
}

bool Utilities::ErrorVariant::isUtilitiesError() const noexcept {
  return canConvert<UtilitiesError>();
}

QString Utilities::ErrorVariant::errorTag() const noexcept {
  if (isSessionError()) {
    return value<Sessions::SessionError>().errorTag();
  } else if (isConnectionError()) {
    return value<Connections::ConnectionError>().errorTag();
  } else if (isUtilitiesError()) {
    return value<UtilitiesError>().errorTag();
  } else {
    return "Unknown";
  }
}

QString Utilities::ErrorVariant::explain() const noexcept {
  if (isSessionError()) {
    return value<Sessions::SessionError>().explain();
  } else if (isConnectionError()) {
    return value<Connections::ConnectionError>().explain();
  } else if (isUtilitiesError()) {
    return value<UtilitiesError>().explain();
  } else {
    return "Unknown";
  }
}
