#ifndef NAMER_HPP
#define NAMER_HPP
#include "UtilitiesError.hpp"
#include "expected.hpp"
#include <QDebug>
#include <QRegularExpression>
#include <QString>
#include <QStringList>
#include <tuple>

namespace Utilities::Namer {

inline auto validateConnectionName(const QString &name) -> bool {
  auto ok = false;
  const auto comp = name.split('.');
  if (comp.length() != 3) {
    return false;
  }
  comp[1].toInt(&ok);
  return ok && !comp[2].isEmpty();
}
inline auto makeUniqueConnectionName(const QString &clientId,
                                     const int sessionId,
                                     const QString &conName)
    -> Result<QString, UtilitiesError> {
  if (clientId.isEmpty() || conName.isEmpty()) {
    UtilitiesError error{
        ErrorType::NotValidNameConnection,
        "Invalid parameters to generate unique connection name, either the "
        "client id is empty or the connection name, the correct schema is "
        "<ClientId>.<SessionId>.<Connection Name>"};
    return Err{error};
  }
  auto name = conName;
  name.replace(
      QRegularExpression{"\\W", QRegularExpression::UseUnicodePropertiesOption},
      "_");
  return QStringLiteral("%1.%2.%3").arg(clientId).arg(sessionId).arg(name);
}
inline auto parseUniqueConnectionName(const QString &name)
    -> Result<std::tuple<QString, int, QString>, UtilitiesError> {
  const auto components = name.split('.');
  if (components.size() != 3) {
    UtilitiesError error{
        ErrorType::NotValidNameConnection,
        QStringLiteral(
            "Invalid connection name to parse from: %1, the correct schema is "
            "<ClientId>.<SessionId>.<Connection Name>, none must be empty")
            .arg(name)};
    return Err{error};
  }
  const auto sessionId = components[0];
  const auto sessionName = components[1].toInt();
  const auto conName = components[2];
  return std::make_tuple(sessionId, sessionName, conName);
}
inline auto getConnectionNamePart(const QString &name)
    -> Result<QString, UtilitiesError> {
  const auto res = parseUniqueConnectionName(name);
  if (res) {
    return std::get<2>(res.value());
  }
  return Err{res.error()};
}
inline auto getSessionIdPart(const QString &name)
    -> Result<int, UtilitiesError> {
  const auto res = parseUniqueConnectionName(name);
  if (res) {
    return std::get<1>(res.value());
  }
  return Err{res.error()};
}
} // namespace Utilities::Namer

#endif // NAMER_HPP
