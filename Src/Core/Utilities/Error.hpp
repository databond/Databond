#ifndef ERRORCODES_HPP
#define ERRORCODES_HPP
#include <QMetaEnum>
#include <QObject>
namespace Sessions {
class SessionError;
}
namespace Connections {
class ConnectionError;
}
namespace Utilities {
Q_NAMESPACE
enum ErrorType {
  NoError,

  // Connections
  CONNECTION_ERROR,
  PluginError,
  ConnectionNotOpened,
  OperationFailed,
  ConnectionNotFound,

  // Sessions & Connection
  SourceError,

  // Sessions
  SESSION_ERROR,
  SessionNotFound,

  // Clients
  CLIENT_ERROR,
  ClientNotFound,
  LimitError,

  // Namer
  NAMER_ERROR,
  NotValidNameConnection
};
Q_ENUM_NS(ErrorType)

class Error {
public:
  Error();
  Error(const ErrorType type, const QString &msg);
  Error(const Error &);
  Error(Error &&);

public:
  virtual QString explain() const noexcept;
  virtual bool isConnectionError() const noexcept;
  virtual bool isUtilityError() const noexcept;
  virtual bool isSessionError() const noexcept;
  virtual bool isClientError() const noexcept;
  virtual QString errorTag() const noexcept;

  virtual auto operator==(const ErrorType type) const noexcept -> bool;
  virtual auto operator!=(const ErrorType type) const noexcept -> bool;
  auto operator=(const Error &) -> Error &;
  auto operator=(Error &&) -> Error &;

public:
  void setError(const ErrorType type, const QString &msg);

  ErrorType type() const noexcept;

private:
  QString mErrorString;
  ErrorType mType;
};

class UtilitiesError;
class ErrorVariant : public QVariant {
public:
  ErrorVariant(const Connections::ConnectionError &er);
  ErrorVariant(const Sessions::SessionError &er);
  ErrorVariant(const UtilitiesError &er);
  auto operator=(const Connections::ConnectionError &er) noexcept
      -> ErrorVariant &;
  auto operator=(const UtilitiesError &er) noexcept -> ErrorVariant &;
  auto operator=(const Sessions::SessionError &er) noexcept -> ErrorVariant &;
  auto isConnectionError() const noexcept -> bool;
  auto isSessionError() const noexcept -> bool;
  auto isUtilitiesError() const noexcept -> bool;
  auto errorTag() const noexcept -> QString;
  auto explain() const noexcept -> QString;

  auto operator==(const ErrorType type) const noexcept -> bool;
  auto operator!=(const ErrorType type) const noexcept -> bool;
};
} // namespace Utilities

#endif // ERRORCODES_HPP
