#include "LocalClient.hpp"
#include "MainWindow.hpp"
#include <QApplication>
#include <QFile>

auto main(int argc, char *args[]) -> int {
  QApplication a{argc, args};
  init();
  QFile file{":/Assets/Themes/MacOS.qss"};
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  a.setStyleSheet(QString::fromUtf8(file.readAll()));
  a.setOrganizationName("Note");
  a.setOrganizationDomain("note.com");
  a.setApplicationName("Databond Local Client");
  MainWindow w;
  w.show();
  return a.exec();
}
