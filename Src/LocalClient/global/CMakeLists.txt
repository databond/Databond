set(SOURCES LocalClient.hpp)
add_library(global ${SOURCES})
target_link_libraries(global Qt${QT_VERSION_MAJOR}::Core Control)
target_include_directories(global PUBLIC .)
