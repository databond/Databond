#ifndef LOCALCLIENT_HPP
#define LOCALCLIENT_HPP
#include <Control/Client.hpp>
#include <QCoreApplication>
#include <QDebug>
using namespace Sessions;
using namespace Connections;
using namespace Utilities;
using namespace Log;
inline Control::Client *GLOBAL_CLIENT;
inline void init(const QString &logFile = "log.txt") {
  GLOBAL_CLIENT = new Control::Client(qApp);
  GLOBAL_CLIENT->call(&Logger::setEnabled, true);
  GLOBAL_CLIENT->call(&Logger::setDevice, new QFile{logFile});
}

inline auto fullConnectionName(const int id, const QString &conName)
    -> QString {
  return QStringLiteral("%1.%2.%3")
      .arg(GLOBAL_CLIENT->id())
      .arg(id)
      .arg(conName);
}
#define MSG_LOG(msg, type) GLOBAL_CLIENT->call(&Logger::log<QString>, msg, type)
#endif // LOCALCLIENT_HPP
