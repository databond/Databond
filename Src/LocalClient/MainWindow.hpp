#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QMap>
#include <QSet>

class Page;
class LogWidget;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  constexpr static inline auto TABLES_INDEX = 0;
  constexpr static inline auto TRIGGERS_INDEX = 1;
  constexpr static inline auto EDITOR_INDEX = 2;
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private:
  void initActions();
  void initSessionTree();
  void initPages();
  void initDbActionButtons();
  void initToolbar();
  void initLogger();
  void connectToUpdateSlot();
  void addPage(Page *page);

private slots:
  // Views
  void switchViewSessionID(bool value);
  void switchViewSessionName(bool value);
  void showConnectionMenu();
  void showSessionMenu();
  void resetActions();

private slots:
  // Actions
  void openSession();
  void newSession();
  void newConnection();
  void queryHistoryDialog();

private:
  auto hasSelection() const -> bool;
  auto isSessionSelected() const -> bool;
  auto isConnectionSelected() const -> bool;
  auto selectedSessionId() const -> int;
  auto selectedConnectionName() const -> QString;

signals:
  void unloaded(int, QString);

private:
  Ui::MainWindow *ui;
};

#endif
