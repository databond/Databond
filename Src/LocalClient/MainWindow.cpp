#include "MainWindow.hpp"
#include "LocalClient.hpp"
#include "dialogs/ConnectionPropertiesDialog/ConnectionPropertiesDialog.hpp"
#include "dialogs/QueryHistoryDialog/QueryHistoryDialog.hpp"
#include "model/SessionTreeModel.hpp"
#include "pages/Page.hpp"
#include "pages/SqlEditor.hpp"
#include "pages/TablesPage.hpp"
#include "pages/TriggersPage.hpp"
#include "state/StateManager.hpp"
#include "ui_MainWindow.h"
#include "utilities/LogWidget.hpp"
#include "wizards/CreateConnectionWizard/CreateConnectionWizard.hpp"
#include "wizards/SetOptionsWizard/SetOptionsWizard.hpp"
#include <QApplication>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>

#include <QStandardPaths>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  initLogger();
  initSessionTree();
  initActions();
  initToolbar();
  initPages();
  setWindowTitle("");
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::initActions() {
  connect(ui->actionFullScreen, &QAction::triggered, [this]() {
    if (isFullScreen()) {
      showNormal();
    } else {
      showFullScreen();
    }
  });
  connect(ui->actionNewSession, &QAction::triggered, this,
          &MainWindow::newSession);
  connect(ui->actionNewConnection, &QAction::triggered, this,
          &MainWindow::newConnection);
  connect(ui->actionOpenFile, &QAction::triggered, this,
          &MainWindow::openSession);
  connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
  connect(ui->actionShowID, &QAction::toggled, this,
          &MainWindow::switchViewSessionID);
  connect(ui->actionShowName, &QAction::toggled, this,
          &MainWindow::switchViewSessionName);
  ui->actionShowID->toggle();
}

void MainWindow::initPages() {
  addPage(new TablesPage{"Tables", this});
  addPage(new TriggersPage{this});
  addPage(new SqlEditor{this});
  ui->pages->setCurrentIndex(TABLES_INDEX);
  connect(ui->pagesComboBox,
          QOverload<int>::of(&QComboBox::currentIndexChanged), ui->pages,
          &QStackedWidget::setCurrentIndex);
  initDbActionButtons();
}

void MainWindow::initDbActionButtons() {
  // Transaction
  connect(ui->transactionBtn, &QPushButton::clicked, []() {
    try {
      const auto current = mGlobalState->getCurrent();
      GLOBAL_CLIENT
          ->call(&ConnectionManager::transaction, current.first, current.second)
          .map([&current](const auto) {
            logInfo(QStringLiteral("%1:%2-> Transaction started")
                        .arg(current.first)
                        .arg(current.second));
          })
          .map_error([&current](const Utilities::ErrorVariant &er) {
            errorLog(QStringLiteral("%1:%2-> %3")
                         .arg(current.first)
                         .arg(current.second)
                         .arg(er.explain()));
          });
    } catch (const std::exception &ex) {
      errorLog(QStringLiteral("No current was set"));
    }
  });

  // Commit
  connect(ui->commitBtn, &QPushButton::clicked, []() {
    try {
      const auto current = mGlobalState->getCurrent();
      GLOBAL_CLIENT
          ->call(&ConnectionManager::commit, current.first, current.second)
          .map([current](const auto v) {
            logInfo(QStringLiteral("%1 - %2: Commited")
                        .arg(current.first)
                        .arg(current.second));
          })
          .map_error([&current](const Utilities::ErrorVariant &er) {
            errorLog(QStringLiteral("%1:%2-> %3")
                         .arg(current.first)
                         .arg(current.second)
                         .arg(er.explain()));
          });
      ;
    } catch (const std::exception &ex) {
      errorLog(QStringLiteral("No current was set"));
    }
  });

  // Rollback
  connect(ui->rollbackBtn, &QPushButton::clicked, []() {
    try {
      const auto current = mGlobalState->getCurrent();
      GLOBAL_CLIENT
          ->call(&ConnectionManager::rollback, current.first, current.second)
          .map([current](const auto v) {
            logInfo(QStringLiteral("%1:%2: Rollback")
                        .arg(current.first)
                        .arg(current.second));
          })
          .map_error([&current](const Utilities::ErrorVariant &er) {
            errorLog(QStringLiteral("%1:%2-> %3")
                         .arg(current.first)
                         .arg(current.second)
                         .arg(er.explain()));
          });
    } catch (const std::exception &ex) {
      errorLog(QStringLiteral("No current was set"));
    }
  });
}
void MainWindow::initToolbar() {
  auto queryHistory = new QAction{"Query history", this};
  connect(queryHistory, &QAction::triggered, this,
          &MainWindow::queryHistoryDialog);
  ui->toolBar->addActions({ui->menuNew->actions() + ui->menuOpen->actions()});
  ui->toolBar->addAction(queryHistory);
}

void MainWindow::initLogger() {
  initLoggerWidget(this);
  ui->dockWidget->setWidget(logger);
}

void MainWindow::addPage(Page *page) {
  ui->pages->addWidget(page);
  connect(ui->updatePageBtn, &QPushButton::clicked, [page]() {
    try {
      const auto &[sessionId, conName] = mGlobalState->getCurrent();
      page->update(sessionId, conName);
    } catch (const std::exception &ex) {
      logWarn(QStringLiteral("No current was set"));
    }
  });
  connect(page, &Page::updated, [](QString pageName, int interval) {
    logInfo(QStringLiteral("Update: %1 in %2 millisecs")
                .arg(pageName)
                .arg(interval));
  });
}

void MainWindow::switchViewSessionID(bool value) {
  ui->sessionTreeView->setColumnHidden(1, !value);
}

void MainWindow::switchViewSessionName(bool value) {
  ui->sessionTreeView->setColumnHidden(0, !value);
}

void MainWindow::initSessionTree() {
  ui->sessionTreeView->setModel(new model::SessionTreeModel{this});
  ui->sessionTreeView->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(ui->sessionTreeView, &QTreeView::customContextMenuRequested,
          [this](const QPoint &) {
            if (isSessionSelected()) {
              showSessionMenu();
            } else if (isConnectionSelected()) {
              showConnectionMenu();
            }
            ui->sessionTreeView->clearSelection();
          });

  connect(ui->sessionTreeView->selectionModel(),
          &QItemSelectionModel::selectionChanged,
          [this](const QItemSelection &, const QItemSelection &) {
            resetActions();
          });

  connect(ui->sessionTreeView, &QTreeView::doubleClicked,
          [this](const QModelIndex &) {
            if (isConnectionSelected()) {
              const auto sessionId = selectedSessionId();
              const auto conName = selectedConnectionName();
              mGlobalState->setCurrent(sessionId, conName);
            }
          });
}

void MainWindow::showConnectionMenu() {
  QAction open{"Open"}, close{"Close"}, remove{"Remove"},
      setOption{"Set connection options"}, properties{"Properties"};
  const auto sessionId = selectedSessionId();
  const auto conName = selectedConnectionName();
  open.setDisabled(mGlobalState->exist(sessionId, conName));

  connect(&open, &QAction::triggered, [sessionId, &conName]() {
    GLOBAL_CLIENT
        ->call(
            QOverload<const int, const QString &>::of(&ConnectionManager::open),
            sessionId, conName)
        .map([sessionId, conName](const auto) {
          logInfo(QStringLiteral("%1.%2 opened").arg(sessionId).arg(conName));
        })
        .map_error([sessionId, conName](const ErrorVariant &er) {
          logInfo(er.explain());
        });
  });

  close.setEnabled(mGlobalState->exist(sessionId, conName));
  connect(&close, &QAction::triggered, [sessionId, &conName]() {
    GLOBAL_CLIENT->call(&ConnectionManager::close, sessionId, conName);
    logInfo(QStringLiteral("%1:%2 closed").arg(sessionId).arg(conName));
  });

  connect(&remove, &QAction::triggered, [sessionId, &conName]() {
    GLOBAL_CLIENT->call(&ConnectionManager::removeConnection, sessionId,
                        conName);
  });

  connect(&setOption, &QAction::triggered, [sessionId, &conName, this]() {
    SetOptionsWizard wizard{sessionId, conName};
    if (wizard.exec() == QDialog::Accepted) {
      const auto options = wizard.options();
      GLOBAL_CLIENT->call(&ConnectionManager::isOpen, sessionId, conName)
          .map([this, sessionId, conName, options](const bool isOp) {
            if (isOp) {
              logWarn("Options must be set before the connection is opened, "
                      "please close and open it again");
            }
            GLOBAL_CLIENT->call(&ConnectionManager::setConnectionOptions,
                                sessionId, conName, options);
            logInfo(QStringLiteral("%1 - %2 options was set")
                        .arg(sessionId)
                        .arg(conName));
          })
          .map_error([sessionId, &conName](const Utilities::ErrorVariant &er) {
            errorLog(QStringLiteral("%1:%2-> %3")
                         .arg(sessionId)
                         .arg(conName)
                         .arg(er.explain()));
          });
      ;
    }
  });

  connect(&properties, &QAction::triggered, [this]() {
    ConnectionPropertiesDialog dialog{selectedSessionId(),
                                      selectedConnectionName(), this};
    dialog.exec();
  });

  QMenu menu;
  menu.addActions({&open, &close, &remove, &setOption, &properties});
  menu.exec(QCursor::pos());
}

void MainWindow::showSessionMenu() {
  QMenu menu;
  QAction save{"Save", this}, close{"Close", this};
  connect(&save, &QAction::triggered, [this]() {
    const auto filename = QFileDialog::getSaveFileName(
        this, "Select Session file",
        QStandardPaths::locate(QStandardPaths::HomeLocation, ".",
                               QStandardPaths::LocateDirectory),
        "Session file"
        "(*.bond)");

    if (!filename.isEmpty()) {
      const auto id = selectedSessionId();
      GLOBAL_CLIENT->call(&SessionManager::saveInFile, id, filename)
          .map([filename, this](const auto) { logInfo("Saved"); })
          .map_error([filename, id](const Utilities::ErrorVariant &er) {
            errorLog(QStringLiteral("Failed to save %1 in %2: -%3")
                         .arg(id)
                         .arg(filename)
                         .arg(er.explain()));
          });
    }
  });

  connect(&close, &QAction::triggered, [this]() {
    const auto sessionId = selectedSessionId();
    GLOBAL_CLIENT->call(&SessionManager::close, sessionId);
    logInfo(QStringLiteral("%1 closed").arg(sessionId));
  });

  menu.addActions({ui->actionNewConnection, &save, &close});
  menu.exec(QCursor::pos());
}

void MainWindow::resetActions() {
  ui->actionNewConnection->setEnabled(isSessionSelected());
}
void MainWindow::openSession() {
  const auto filename = QFileDialog::getOpenFileName(
      this, "Open Session File",
      QStandardPaths::locate(QStandardPaths::HomeLocation, ".",
                             QStandardPaths::LocateDirectory),
      "Session file (*.bond)");
  if (!filename.isEmpty()) {
    GLOBAL_CLIENT
        ->call(&SessionManager::newSessionFromFile<Connections::DBConnection>,
               filename, &GLOBAL_CLIENT->mConnection)
        .map([](const auto) { logInfo("Session was loaded"); })
        .map_error([](const Utilities::ErrorVariant &er) {
          errorLog(QStringLiteral("Error in loading session -> %1")
                       .arg(er.explain()));
        });
    ;
  }
}

void MainWindow::newSession() {
  auto ok = false;
  const auto name = QInputDialog::getText(
      this, "New Session", "Session Name", QLineEdit::Normal, "", &ok,
      Qt::WindowFlags{}, Qt::ImhPreferUppercase);
  if (name.isEmpty() || !ok) {
    return;
  }
  GLOBAL_CLIENT->call(
      QOverload<const QString &>::of(&SessionManager::newSession), name);
}

void MainWindow::newConnection() {
  const auto sessionId = selectedSessionId();
  const auto conName = selectedConnectionName();
  CreateConnectionWizard wizard{sessionId, conName};
  if (wizard.exec() != QDialog::Accepted) {
    return;
  }

  QJsonObject obj{{"ConnectionName", wizard.connectionName()},
                  {"DatabaseName", wizard.database()},
                  {"HostName", wizard.host()},
                  {"Options", wizard.options()},
                  {"Password", wizard.password()},
                  {"PluginName", wizard.pluginName()},
                  {"Port", wizard.port()},
                  {"Username", wizard.username()}};
  GLOBAL_CLIENT->call(QOverload<const int, const QJsonObject &>::of(
                          &ConnectionManager::addConnection),
                      selectedSessionId(), obj);
}

void MainWindow::queryHistoryDialog() {
  QueryHistoryDialog queryHistoryDialog;
  if (queryHistoryDialog.exec() == QDialog::Accepted) {
    qobject_cast<SqlEditor *>(ui->pages->widget(EDITOR_INDEX))
        ->setQuery(queryHistoryDialog.getImportedQuery());
  }
}

bool MainWindow::hasSelection() const {
  return ui->sessionTreeView->selectionModel()->hasSelection();
}

bool MainWindow::isSessionSelected() const {
  if (!hasSelection()) {
    return false;
  }
  const auto selectedItem =
      ui->sessionTreeView->selectionModel()->selectedIndexes().first();
  return !selectedItem.parent().isValid();
}

bool MainWindow::isConnectionSelected() const {
  if (!hasSelection()) {
    return false;
  }
  return !isSessionSelected();
}

int MainWindow::selectedSessionId() const {
  auto selected = ui->sessionTreeView->selectionModel()->selectedRows();
  if (isSessionSelected()) {
    return selected.first().siblingAtColumn(1).data().toInt();
  } else if (isConnectionSelected()) {
    return selected.first().parent().siblingAtColumn(1).data().toInt();
  }
  return -1;
}

QString MainWindow::selectedConnectionName() const {
  return isConnectionSelected() ? ui->sessionTreeView->selectionModel()
                                      ->selectedRows()
                                      .first()
                                      .data()
                                      .toString()
                                : "";
}
