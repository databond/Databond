#ifndef TABLESSTATE_HPP
#define TABLESSTATE_HPP
#include <LocalClient.hpp>
#include <QList>
#include <QObject>
#include <QSqlRelationalTableModel>
#include <QString>

class QSqlRelationalTableModel;

class TablesState : public QObject {
  Q_OBJECT
public:
  TablesState(const int sessionId, const QString &conName,
              const QString &tableType, QObject *parent = nullptr);
  ~TablesState();

  private:
  void clear();
  void shrink();
  public:
  void updateTables();
  void initTableModel(QSqlRelationalTableModel *model, const QString &table);

  QList<QSqlRelationalTableModel *> mModels;
  int mSessionId;
  QString mConName, mTableType;
};

#endif // TABLESSTATE_HPP
