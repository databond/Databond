#ifndef EDITORSTATE_HPP
#define EDITORSTATE_HPP

#include <QObject>

class SqlHighlighter;
class QSqlQueryModel;

class EditorState : public QObject
{
    Q_OBJECT
public:
    explicit EditorState(const int sessionId, const QString &conName, QObject *parent = nullptr);

    ~EditorState();

private:
    QString findStyleFile(const QString &pluginName);

public:
    QString mSql, mLog;
    int mSessionId;
    QString mConName;
    SqlHighlighter *mHighlighter;
    QSqlQueryModel *mModel;
};

#endif // EDITORSTATE_HPP
