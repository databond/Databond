#ifndef TRIGGERSSTATE_HPP
#define TRIGGERSSTATE_HPP

#include <QObject>

class QStringListModel;

class TriggersState : public QObject {
  Q_OBJECT
public:
  explicit TriggersState(const int sessionId, const QString &conName,
                         QObject *parent = nullptr);

  QStringListModel *mModel;
  int mSessionId;
  QString mConName;
};

#endif // TRIGGERSSTATE_HPP
