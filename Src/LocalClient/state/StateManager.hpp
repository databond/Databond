#ifndef STATE_HPP
#define STATE_HPP
#include <QMap>
#include <QMutex>
#include <QSet>
#include <QSharedPointer>
#include <QString>

class TablesState;
class TriggersState;
class EditorState;

class StateManager : public QObject {
  Q_OBJECT
public:
  using StatesType =
      std::tuple<TablesState *, TablesState *, TriggersState *, EditorState *>;
  using StateType = QMap<int, QMap<QString, StatesType>>;

public:
  explicit StateManager(QObject *parent = nullptr);

public:
  auto exist(const int sessionId, const QString &conName) const -> bool;
  auto tablesState(const int sessionId, const QString &conName,
                   const QString &tableType = "Tables") -> TablesState &;
  auto triggersState(const int sessionId, const QString &conName)
      -> TriggersState &;
  auto editorState(const int sessioId, const QString &conName) -> EditorState &;

  auto states(const int sessionId, const QString &conName) -> StatesType &;
  auto setCurrent(const int sessionId, const QString &conName) -> void;
  auto getCurrent() const -> QPair<int, QString>;

  static auto instance() -> QSharedPointer<StateManager> {
    static QSharedPointer<StateManager> state{new StateManager};
    return state;
  }

private:
  auto loadState(const int sessionId, const QString &conName) -> StatesType;

signals:
  void currentChanged(int, QString);
  void removed(int, QString);

private:
  QMutex mMutex;
  StateType mState;
  QPair<int, QString> mCurrent;
};

#define mGlobalState StateManager::instance()

#endif // STATE_HPP
