#include "EditorState.hpp"
#include "../pages/Highlighter/SyntaxHightlighter.hpp"
#include <LocalClient.hpp>
#include <QApplication>
#include <QSqlQueryModel>

EditorState::EditorState(const int sessionId, const QString &conName,
                         QObject *parent)
    : QObject(parent), mSessionId{sessionId}, mConName{conName},
      mHighlighter{new SqlHighlighter{this}}, mModel{new QSqlQueryModel{this}} {

  mHighlighter->loadFromFile(findStyleFile(
      QSqlDatabase::database(fullConnectionName(sessionId, conName))
          .driverName()));
}

EditorState::~EditorState() {}

QString EditorState::findStyleFile(const QString &pluginName) {
  const auto styleDir = qApp->applicationDirPath() + "/styles/";
  const auto filename = styleDir + pluginName + "Style.json";
  return filename;
}
