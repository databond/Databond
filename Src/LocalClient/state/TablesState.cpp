
#include "TablesState.hpp"
#include <LocalClient.hpp>
#include <QApplication>
#include <QSqlRelationalTableModel>

TablesState::TablesState(const int sessionId, const QString &conName,
                         const QString &tableType, QObject *parent)
    : QObject{parent}, mSessionId{sessionId}, mConName{conName},
      mTableType{tableType} {
  if (GLOBAL_CLIENT->call(&ConnectionManager::exist, sessionId, conName)) {
    if (const auto tables = GLOBAL_CLIENT->call(&ConnectionManager::tables,
                                                sessionId, conName, mTableType);
        tables.has_value()) {
      for (const auto &table : tables.value()) {
        auto db =
            QSqlDatabase::database(fullConnectionName(sessionId, conName));
        auto ptr{new QSqlRelationalTableModel{this, db}};
        initTableModel(ptr, table);
        mModels.push_back(ptr);
      }
    }
  }
}
void TablesState::clear() {
  for (auto &model : mModels) {
    model->deleteLater();
  }
  mModels.clear();
}
void TablesState::shrink() {
  GLOBAL_CLIENT
      ->call(&ConnectionManager::tables, mSessionId, mConName, mTableType)
      .map([this](const QStringList &tables) {
        for (auto &model : mModels) {
          if (!tables.contains(model->tableName())) {
            mModels.removeOne(model);
            model->deleteLater();
          }
        }
      });
}
void TablesState::initTableModel(QSqlRelationalTableModel *model,
                                 const QString &table) {
  model->setTable(table);
  model->setEditStrategy(QSqlTableModel::OnManualSubmit);
  model->select();
  connect(model, &QSqlRelationalTableModel::dataChanged,
          [model]() { model->submitAll(); });
}
void TablesState::updateTables() {
  shrink();
  GLOBAL_CLIENT
      ->call(&ConnectionManager::tables, mSessionId, mConName, mTableType)
      .map([this](const QStringList &tables) {
        if (tables.size() == mModels.size()) {
          return;
        } else {
          QStringList localTables;
          for (const auto &model : mModels) {
            localTables.push_back(model->tableName());
          }
          for (const auto &table : tables) {
            if (!localTables.contains(table)) {
              auto model = new QSqlRelationalTableModel{
                  this, QSqlDatabase::database(
                            fullConnectionName(mSessionId, mConName))};
              initTableModel(model, table);
              mModels.push_back(model);
            }
          }
        }
      });
}

TablesState::~TablesState() { clear(); }
