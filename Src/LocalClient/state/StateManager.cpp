#include "StateManager.hpp"
#include "../utilities/LogWidget.hpp"
#include "EditorState.hpp"
#include "TablesState.hpp"
#include "TriggersState.hpp"
#include <LocalClient.hpp>
#include <QDebug>

StateManager::StateManager(QObject *parent) : QObject{parent} {
  connect(
      &GLOBAL_CLIENT->mSession, &SessionManager::sessionCreated, this,
      [this](int sessionId) { mState[sessionId] = {}; }, Qt::QueuedConnection);
  connect(
      &GLOBAL_CLIENT->mSession, &SessionManager::sessionClosed, this,
      [this](int sessionId) { mState.remove(sessionId); },
      Qt::QueuedConnection);
  connect(
      &GLOBAL_CLIENT->mConnection, &ConnectionManager::connectionClosed, this,
      [this](int sessionId, QString conName) {
        auto &state = mState[sessionId][conName];
        std::get<0>(state)->deleteLater();
        std::get<1>(state)->deleteLater();
        std::get<2>(state)->deleteLater();
        std::get<3>(state)->deleteLater();
        mState[sessionId].remove(conName);
        if (mCurrent == QPair<int, QString>{sessionId, conName}) {
          emit removed(sessionId, conName);
          mCurrent = {-1, ""};
        }
      },
      Qt::QueuedConnection);
  connect(
      &GLOBAL_CLIENT->mConnection, &ConnectionManager::connectionOpened, this,
      [this](int sessionId, QString conName) {
        if (mState.contains(sessionId)) {
          if (mState[sessionId].contains(conName)) {
            logWarn(QStringLiteral("%1 - %2 already opened")
                        .arg(sessionId)
                        .arg(conName));
            return;
          } else {
            mState[sessionId].insert(conName, loadState(sessionId, conName));
          }
        } else {
          mState.insert(sessionId,
                        {{{conName, loadState(sessionId, conName)}}});
        }
      },
      Qt::QueuedConnection);
}

bool StateManager::exist(const int sessionId, const QString &conName) const {
  if (mState.contains(sessionId)) {
    if (mState[sessionId].contains(conName)) {
      return true;
    }
  }
  return false;
}

TablesState &StateManager::tablesState(const int sessionId,
                                       const QString &conName,
                                       const QString &tableType) {
  return tableType == "Tables" ? *std::get<0>(states(sessionId, conName))
                               : *std::get<1>(states(sessionId, conName));
}

TriggersState &StateManager::triggersState(const int sessionId,
                                           const QString &conName) {
  return *std::get<2>(states(sessionId, conName));
}

EditorState &StateManager::editorState(const int sessionId,
                                       const QString &conName) {
  return *std::get<3>(states(sessionId, conName));
}

StateManager::StatesType &StateManager::states(const int sessionId,
                                               const QString &conName) {
  return mState[sessionId][conName];
}

void StateManager::setCurrent(const int sessionId, const QString &conName) {
  QMutexLocker locker{&mMutex};
  if (mCurrent == QPair<int, QString>{sessionId, conName}) {
    return;
  }
  if (mState.contains(sessionId) && mState[sessionId].contains(conName)) {
    mCurrent = {sessionId, conName};
    emit currentChanged(sessionId, conName);
  } else {
    logWarn(QStringLiteral("%1:%2: Please open it first")
                .arg(sessionId)
                .arg(conName));
  }
}

QPair<int, QString> StateManager::getCurrent() const {
  if (mState.contains(mCurrent.first) &&
      mState[mCurrent.first].contains(mCurrent.second)) {
    return mCurrent;
  }
  throw std::runtime_error{"No Current was set"};
}

StateManager::StatesType StateManager::loadState(const int sessionId,
                                                 const QString &conName) {
  auto tablesState = new TablesState{sessionId, conName, "Tables"};
  auto viewsState = new TablesState{sessionId, conName, "Views"};
  auto triggerState = new TriggersState{sessionId, conName};
  auto editorState = new EditorState{sessionId, conName};

  auto tuple =
      std::make_tuple(tablesState, viewsState, triggerState, editorState);
  return tuple;
}
