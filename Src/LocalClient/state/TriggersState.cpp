#include "TriggersState.hpp"
#include <LocalClient.hpp>
#include <QStringListModel>

TriggersState::TriggersState(const int sessionId, const QString &conName,
                             QObject *parent)
    : QObject(parent), mModel{new QStringListModel{this}},
      mSessionId{sessionId}, mConName{conName} {
  GLOBAL_CLIENT->call(&ConnectionManager::triggers, sessionId, conName)
      .map([this](const QStringList &triggers) {
        mModel->setStringList(triggers);
      });
}
