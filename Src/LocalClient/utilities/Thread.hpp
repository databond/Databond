#ifndef THREAD_H
#define THREAD_H
#include "../LocalClient.hpp"
#include <QDeferred>
#include <QString>
#include <functional>

namespace Thread {
inline auto execute(const QPair<int, QString> &sc, std::function<void()> func,
                    bool async = true) -> void {
  QDeferred<bool> def;
  GLOBAL_CLIENT->resolvedConnectionCall<void>(
      &CM::executeConnectionThread, sc.first, sc.second, [def, func]() mutable {
        func();
        def.resolve(true);
      });
  if (!async) {
    QDefer::await(def);
  }
}
inline auto executeInMainThread(const std::function<void()> func) -> void {
  QDeferred<bool> def;
  QLambdaThreadWorker thread;
  thread.execInThread([&func, def]() mutable {
    auto timer = new QTimer{};
    timer->setSingleShot(true);
    timer->moveToThread(qApp->thread());
    timer->setParent(qApp);
    QObject::connect(timer, &QTimer::timeout, [=]() mutable {
      func();
      timer->deleteLater();
    });
    QMetaObject::invokeMethod(timer, "start", Qt::QueuedConnection,
                              Q_ARG(int, 0));
  });
  QDefer::await(def);
}
} // namespace Thread
#endif // THREAD_H
