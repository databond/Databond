#ifndef LOGWIDGET_HPP
#define LOGWIDGET_HPP

#include <QTextEdit>
namespace Log{
class LogWidget : public QTextEdit
{
public:
    LogWidget(QWidget *parent = nullptr);

public slots:
    void logSuccess(QString msg);
    auto logError(QString msg) -> void;
    auto logWarning(QString msg) -> void;

};
inline LogWidget* logger;
#define errorLog(msg) logger->logError(msg)
#define logWarn(msg) logger->logWarning(msg)
#define logInfo(msg) logger->logSuccess(msg)
}

inline void initLoggerWidget(QWidget* parent = nullptr){
    Log::logger = new Log::LogWidget{parent};
}

#endif // LOGWIDGET_HPP
