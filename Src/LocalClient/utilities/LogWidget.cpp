#include "LogWidget.hpp"
#include <LocalClient.hpp>

#include <QScrollBar>
LogWidget::LogWidget(QWidget *parent) : QTextEdit{parent} {
  setReadOnly(true);
  setFontPointSize(50);
}

void LogWidget::logSuccess(QString msg) {
  insertHtml(QStringLiteral("<p><font color='blue' size='4'>%1</font></p><br>")
                 .arg(msg));
  moveCursor(QTextCursor::End);
  MSG_LOG(msg + '\n', LogType::Info);
}

void LogWidget::logError(QString msg) {
  insertHtml(
      QStringLiteral("<p><font color='red' size='4'>Failure: %1</font></p><br>")
          .arg(msg));
  moveCursor(QTextCursor::End);
  MSG_LOG(msg + '\n', LogType::Error);
}

void LogWidget::logWarning(QString msg) {
  insertHtml(QStringLiteral(
                 "<p><font color='green' size='4'>Warning: %1</font></p><br>")
                 .arg(msg));
  moveCursor(QTextCursor::End);
  MSG_LOG(msg + '\n', LogType::Warn);
}
