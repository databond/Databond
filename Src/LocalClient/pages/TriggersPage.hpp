#ifndef TRIGGERSPAGE_HPP
#define TRIGGERSPAGE_HPP

#include "Page.hpp"
#include <QMap>

class QStringListModel;

namespace Ui {
class TriggersPage;
}

class TriggersPage : public Page {
  Q_OBJECT

public:
  explicit TriggersPage(QWidget *parent = nullptr);
  ~TriggersPage();

private:
  Ui::TriggersPage *ui;

  // Page interface
public slots:
  void updateCurrent(int, QString) override;
  void unload(int, QString) override;
  void load(int, QString) override;

private:
  QMap<QPair<int, QString>, QStringListModel *> mModel;
  QPair<int, QString> mCurrent;
};

#endif // TRIGGERSPAGE_HPP
