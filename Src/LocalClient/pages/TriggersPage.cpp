#include "TriggersPage.hpp"
#include "../state/StateManager.hpp"
#include "../state/TriggersState.hpp"
#include "ui_TriggersPage.h"
#include <LocalClient.hpp>
#include <QStringListModel>
#include <chrono>

TriggersPage::TriggersPage(QWidget *parent)
    : Page(parent), ui(new Ui::TriggersPage) {
  ui->setupUi(this);
  setPageName("Triggers");
}

TriggersPage::~TriggersPage() { delete ui; }

void TriggersPage::load(int sessionId, QString conName) {
  const auto &state = mState->triggersState(sessionId, conName);
  ui->triggersList->setModel(state.mModel);
}

void TriggersPage::updateCurrent(int sessionId, QString conName) {

  GLOBAL_CLIENT->call(&ConnectionManager::triggers, sessionId, conName)
      .map([this, sessionId, conName](const QStringList &triggers) {
        mState->triggersState(sessionId, conName)
            .mModel->setStringList(triggers);
      });
}

void TriggersPage::unload(int, QString) { ui->triggersList->setModel(nullptr); }
