#include "SqlEditor.hpp"
#include "../state/EditorState.hpp"
#include "../state/StateManager.hpp"
#include "../utilities/LogWidget.hpp"
#include "Highlighter/SyntaxHightlighter.hpp"
#include "QApplication"
#include "ui_SqlEditor.h"
#include <LocalClient.hpp>
#include <QCompleter>
#include <QDir>
#include <QDirIterator>
#include <QListView>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QStringListModel>
inline auto addToSettings(const QVariant &v) -> void {
  QSettings settings;
  auto queries =
      settings.value("Databond/LocalClient/QueryHistory", {}).toStringList();
  queries.append(v.toString());
  queries.removeDuplicates();
  settings.setValue("Databond/LocalClient/QueryHistory", queries);
}

using namespace std::chrono;

SqlEditor::SqlEditor(QWidget *parent) : Page(parent), ui(new Ui::SqlEditor) {
  ui->setupUi(this);
  setPageName("Sql Editor");
  connect(ui->executeBtn, &QPushButton::clicked, this,
          &SqlEditor::executeQuery);
  // mCompleter->setCompletionMode(QCompleter::PopupCompletion);
  // qobject_cast<QListView
  // *>(mCompleter->popup())->setModel(mCompleter->model());
  // TODO: missing completer functionality
}

SqlEditor::~SqlEditor() { delete ui; }

void SqlEditor::load(int sessionId, QString conName) {
  auto &state = mState->editorState(sessionId, conName);
  ui->sqlTextEdit->setHtml(state.mSql);
  ui->logTextBrowser->setHtml(state.mLog);
  state.mHighlighter->setDocument(ui->sqlTextEdit->document());
  ui->resultTableView->setModel(state.mModel);
}

void SqlEditor::executeQuery() {
  try {
    const auto &currentSqlText = ui->sqlTextEdit->toPlainText();
    const auto &current = mState->getCurrent();

    auto &state = mState->editorState(current.first, current.second);
    state.mSql = currentSqlText;
    const auto start = high_resolution_clock::now();

    state.mModel->setQuery(
        state.mSql, QSqlDatabase::database(
                        fullConnectionName(current.first, current.second)));

    const auto interval =
        duration_cast<milliseconds>(high_resolution_clock::now() - start)
            .count();
    if (state.mModel->lastError().type() == QSqlError::NoError) {
      state.mLog =
          QStringLiteral("Executed successfully in %1 millisec").arg(interval);
      addToSettings(state.mSql);
    } else {
      state.mLog = QStringLiteral("In %1 millisec\n failed to execute: %2")
                       .arg(interval)
                       .arg(state.mModel->lastError().text());
    }
    if (mState->getCurrent() == current) {
      load(current.first, current.second);
    }
  } catch (const std::exception &ex) {
    logWarn(ex.what());
  }
}

void SqlEditor::setQuery(const QString &query) {
  ui->sqlTextEdit->setText(query);
}

void SqlEditor::updateCurrent(int, QString) {
  // updateCompleter();
}

void SqlEditor::unload(int, QString) {
  ui->logTextBrowser->clear();
  ui->sqlTextEdit->clear();
  ui->resultTableView->setModel(nullptr);
}
