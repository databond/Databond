#include "TablesPage.hpp"
#include "../state/TablesState.hpp"
#include "../utilities/LogWidget.hpp"
#include "Diagram/Diagram.h"
#include "ui_TablesPage.h"
#include <LocalClient.hpp>
#include <QImage>
#include <QLabel>
#include <QProcess>
#include <QSqlRelationalTableModel>
TablesPage::TablesPage(const QString &tableType, QWidget *parent)
    : Page{parent}, ui(new Ui::TablesPage), mTableType{tableType} {
  ui->setupUi(this);
  if (mTableType == "Views") {
    ui->addBtn->setEnabled(false);
    delete ui->genDiagramsBtn;
  } else {
    connect(ui->genDiagramsBtn, &QPushButton::clicked, this,
            &TablesPage::generateTableDiagrams);
  }
  setPageName(mTableType);
  connect(ui->tablesComboBox, &QComboBox::currentTextChanged, this,
          &TablesPage::setCurrentTable);
  connect(ui->addBtn, &QPushButton::clicked, this, &TablesPage::appendRow);
  connect(ui->removeBtn, &QPushButton::clicked, this, &TablesPage::removeRows);
  connect(ui->filterEdit, &QLineEdit::returnPressed, this,
          &TablesPage::filterTable);
  ui->tableView->setAutoScroll(false);
}

TablesPage::~TablesPage() { delete ui; }

void TablesPage::updateCurrent(int sessionId, QString conName) {
  auto &state = mState->tablesState(sessionId, conName, mTableType);
  state.updateTables();
  const auto currentTable = ui->tablesComboBox->currentText();
  ui->tablesComboBox->clear();
  QStringList tables;
  for (const auto &model : state.mModels) {
    tables += model->tableName();
  }
  for (auto &model : state.mModels) {
    model->select();
  }
  ui->tablesComboBox->addItems(tables);
  ui->tablesComboBox->setCurrentText(currentTable);
}

void TablesPage::load(int sessionId, QString conName) {
  const auto &state = mState->tablesState(sessionId, conName, mTableType);
  ui->tablesComboBox->clear();
  QStringList tables;
  for (const auto &model : state.mModels) {
    tables += model->tableName();
  }
  ui->tablesComboBox->addItems(tables);
}

void TablesPage::setCurrentTable(QString table) {
  try {
    const auto &[sessionId, conName] = mState->getCurrent();
    for (const auto &model :
         mState->tablesState(sessionId, conName, mTableType).mModels) {
      if (table == model->tableName()) {
        ui->tableView->setModel(model);
      }
    }
  } catch (const std::exception &ex) {
    logWarn(ex.what());
  }
  ui->filterEdit->clear();
}
void TablesPage::filterTable() {
  auto model = qobject_cast<QSqlRelationalTableModel *>(ui->tableView->model());
  if (!model) {
    return;
  }
  const auto filter = ui->filterEdit->text().trimmed();
  model->setFilter(filter.isEmpty() ? "1=1" : filter);
  model->select();
}

void TablesPage::unload(int, QString) {
  ui->tablesComboBox->clear();
  ui->tableView->setModel(nullptr);
}

void TablesPage::appendRow() {
  if (auto model =
          qobject_cast<QSqlRelationalTableModel *>(ui->tableView->model());
      model) {
    model->insertRow(model->rowCount());
  }
}

void TablesPage::removeRows() {
  if (auto selectionModel = ui->tableView->selectionModel();
      selectionModel && selectionModel->hasSelection()) {
    const auto &rows = selectionModel->selectedRows();
    const auto pos = rows.first().row();
    const auto count = rows.count();
    auto model =
        qobject_cast<QSqlRelationalTableModel *>(ui->tableView->model());

    model->removeRows(pos, count);
    model->submitAll();
    model->select();
  }
}

void TablesPage::generateTableDiagrams() {
  try {
    const auto current = mState->getCurrent();
    GLOBAL_CLIENT
        ->call(&ConnectionManager::tables, current.first, current.second,
               "tables")
        .map([&current, this](const QStringList &tables) {
          auto diagram =
              new DisplayDiagram{current.first, current.second, tables, this};
          diagram->show();
        });
  } catch (const std::exception &ex) {
    logWarn(ex.what());
  }
}
