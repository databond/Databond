#ifndef TABLESPAGE_HPP
#define TABLESPAGE_HPP

#include "../state/StateManager.hpp"
#include "Page.hpp"
#include <QMap>
#include <QSet>

class QItemSelection;
namespace Ui {
class TablesPage;
}

class TablesPage : public Page {
  Q_OBJECT

public:
  explicit TablesPage(const QString &tableType, QWidget *parent = nullptr);
  ~TablesPage();

public slots:
  void updateCurrent(int sessionId, QString conName) override;
  void load(int sessionId, QString conName) override;
  void unload(int, QString) override;
  void appendRow();
  void removeRows();
  void generateTableDiagrams();

private slots:
  void setCurrentTable(QString table);
  void filterTable();

private:
  Ui::TablesPage *ui;

private:
  const QString mTableType;
};

#endif // TABLESPAGE_HPP
