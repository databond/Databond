#ifndef SQLEDITOR_HPP
#define SQLEDITOR_HPP

#include "Page.hpp"
#include <QMap>

class SqlHighlighter;
class QSqlQueryModel;
class QCompleter;
namespace Ui {
class SqlEditor;
}
struct EditorState;

class SqlEditor : public Page {
  Q_OBJECT
public:
  explicit SqlEditor(QWidget *parent = nullptr);
  ~SqlEditor();

private:
  Ui::SqlEditor *ui;

  // Page interface
public slots:
  void updateCurrent(int, QString) override;
  void unload(int, QString) override;
  void load(int, QString) override;
  void executeQuery();

public:
  auto setQuery(const QString &query) -> void;

private:
  QCompleter *mCompleter;
};

#endif // SQLEDITOR_HPP
