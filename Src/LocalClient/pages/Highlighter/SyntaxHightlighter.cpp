#include "SyntaxHightlighter.hpp"
#include "../utilities/LogWidget.hpp"
#include "qstringliteral.h"
#include <LocalClient.hpp>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>

inline const QString DEFAULT_COLOR = QStringLiteral("black");
constexpr inline auto DEFAULT_FONT_SIZE = 10;
inline const QString DEFAULT_STYLE = QStringLiteral("regular");
inline const QString DEFAULT_QUOTED_TEXT_COLOR = QStringLiteral("orange");

SqlHighlighter::SqlHighlighter(QObject *parent) : QSyntaxHighlighter{parent} {
  QFile file{QStringLiteral(":/sheets/Assets/Json/StyleSchema.json")};
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  mSchema = new JsonSchema{
      JsonSchema::fromJsonDocument(QJsonDocument::fromJson(file.readAll()))};
}

void SqlHighlighter::loadFromFile(QString filename) {
  auto defaultLoaded = false;
  QFile file{filename};
  QFileInfo info {filename};
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    errorLog(QStringLiteral("Failed to open json style file %1 -> %2")
             .arg(info.baseName())
                 .arg(file.errorString()));
    defaultLoaded = true;
    filename = ":/sheets/Assets/Json/DefaultStyle.json";
    file.setFileName(filename);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
  }

  mSource = file.readAll();
  if (!mSchema->validate(mSource.toUtf8())) {
    logWarn(QStringLiteral("The file %1 does not confrom to the style schem")
                .arg(info.baseName()));
  }
  mKeywords.clear();

  mQuoteFormat = QTextCharFormat{};
  proccessSource();
  if (defaultLoaded) {
    logInfo("Default style was loaded");
  } else {
    logInfo(QStringLiteral("Style file %1 was loaded").arg(info.baseName()));
  }
}

QString SqlHighlighter::source() { return mSource; }

QStringList SqlHighlighter::keywords() { return mKeywords.keys(); }

void SqlHighlighter::highlightBlock(const QString &text) {
  QTextCharFormat globalFormat;
  globalFormat.setFontPointSize(mDefaultFontSize);
  globalFormat.setForeground(QBrush{QColor{mDefaultColor}});
  if (mDefaultStyle == "bold") {
    globalFormat.setFontWeight(QFont::Weight::Bold);
  } else if (mDefaultStyle == "regular") {
    globalFormat.setFontWeight(QFont::Weight::Normal);
  } else {
    globalFormat.setFontItalic(true);
  }
  setFormat(0, text.length(), globalFormat);
  for (const auto &[name, format] : mKeywords.toStdMap()) {
    QRegularExpression expression{QStringLiteral("\\b%1\\b").arg(name)};
    if (!mIsCaseSensitive) {
      expression.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
    }
    auto iter = expression.globalMatch(text);
    while (iter.hasNext()) {
      auto match = iter.next();
      setFormat(match.capturedStart(), match.capturedLength(), format);
    }
  }
  QRegularExpression quotesExpression{QStringLiteral("(\"([^\"]|\"\")*\")")};
  quotesExpression.setPatternOptions(
      QRegularExpression::ExtendedPatternSyntaxOption);
  auto iter = quotesExpression.globalMatch(text);
  while (iter.hasNext()) {
    auto match = iter.next();
    setFormat(match.capturedStart(), match.capturedLength(), mQuoteFormat);
  }
}

void SqlHighlighter::proccessSource() {
  const auto obj = QJsonDocument::fromJson(mSource.toUtf8()).object();
  mIsCaseSensitive = obj["CaseSensitive"].toBool();
  const auto sheetObj = obj["Sheet"].toObject();
  mDefaultColor = sheetObj["Color"].toString(DEFAULT_COLOR);
  mDefaultFontSize = sheetObj["Size"].toInt(DEFAULT_FONT_SIZE);
  mDefaultStyle = sheetObj["FontStyle"].toString(DEFAULT_STYLE);
  mDefaultQuotedTextColor =
      sheetObj["QuotedText-Color"].toString(DEFAULT_QUOTED_TEXT_COLOR);
  mQuoteFormat.setForeground(QColor{mDefaultQuotedTextColor});
  mQuoteFormat.setFontPointSize(mDefaultFontSize);
  const auto keywordsArray = sheetObj["Keywords"].toArray();
  for (const auto &keyword : keywordsArray) {
    QTextCharFormat fmt;
    QString name = "n/a";
    if (keyword.isString()) {
      if (mDefaultStyle == "bold") {
        fmt.setFontWeight(QFont::Weight::Bold);
      } else if (mDefaultStyle == "regular") {
        fmt.setFontWeight(QFont::Weight::Normal);
      } else {
        fmt.setFontItalic(true);
      }
      fmt.setFontPointSize(mDefaultFontSize);
      fmt.setForeground(QColor{mDefaultColor});
      name = keyword.toString();
    } else if (const auto kObj = keyword.toObject(); keyword.isObject()) {
      if (const auto style = kObj["Style"].toString(mDefaultStyle);
          style == "bold") {
        fmt.setFontWeight(QFont::Weight::Bold);
      } else if (style == "regular") {
        fmt.setFontWeight(QFont::Weight::Normal);
      } else {
        fmt.setFontItalic(true);
      }
      fmt.setFontPointSize(kObj["Size"].toInt(mDefaultFontSize));
      fmt.setForeground(QColor{kObj["Color"].toString(mDefaultColor)});
      name = kObj["Name"].toString("n/a");
    }
    mKeywords[name] = fmt;
  }
}
