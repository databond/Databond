#ifndef SQLHIGHLIGHTER_HPP
#define SQLHIGHLIGHTER_HPP

#include <QHash>
#include <QRegularExpression>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QtJsonSchema>

class QDomDocument;

class SqlHighlighter : public QSyntaxHighlighter {
  Q_OBJECT
public:
  SqlHighlighter(QObject *parent = nullptr);

public:
  auto loadFromFile(QString filename) -> void;
  auto source() -> QString;
  auto keywords() -> QStringList;

  // QSyntaxHighlighter interface
protected:
  void highlightBlock(const QString &text) override;

private:
  auto proccessSource() -> void;

private:
  QString mSource;
  QMap<QString, QTextCharFormat> mKeywords;
  QTextCharFormat mQuoteFormat;
  JsonSchema *mSchema;
  bool mIsCaseSensitive = false;
  QString mDefaultColor;
  int mDefaultFontSize;
  QString mDefaultStyle;
  QString mDefaultQuotedTextColor;
};

#endif // SQLHIGHLIGHTER_HPP
