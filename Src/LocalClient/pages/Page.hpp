#ifndef PAGE_HPP
#define PAGE_HPP
#include <QMutex>
#include <QWidget>

class StateManager;

class Page : public QWidget {
  Q_OBJECT
public:
  explicit Page(QWidget *parent = nullptr);

  QString pageName() const;
  void setPageName(const QString &newPageName);

public slots:

  void update(int sessionId, QString conName);

  /**
   * @brief Update all page instances of the given session, connection
   */
  virtual void updateCurrent(int, QString) = 0;

  /**
   * @brief Load an instance for a specified session/connection
   */
  virtual void load(int, QString) = 0;

  /**
   * @brief unload the instance for the specified session/connection
   */
  virtual void unload(int, QString) = 0;

signals:
  void updated(QString pageName, int millSecPeriod);

private:
  QString mPageName;

protected:
  using SessionConnectionType = QPair<int, QString>;
  QSharedPointer<StateManager> mState;
};

#endif // PAGE_HPP
