#include "Page.hpp"
#include "../state/StateManager.hpp"
#include <LocalClient.hpp>
#include <QApplication>
#include <QMutexLocker>
#include <QTimer>
#include <chrono>

Page::Page(QWidget *parent) : QWidget(parent), mState{mGlobalState} {
  connect(mState.get(), &StateManager::currentChanged, this, &Page::load);
  connect(mState.get(), &StateManager::removed, this, &Page::unload);
}

QString Page::pageName() const { return mPageName; }

void Page::setPageName(const QString &newPageName) { mPageName = newPageName; }

void Page::update(int sessionId, QString conName) {
  if (mState->exist(sessionId, conName)) {
    const auto startPoint = std::chrono::high_resolution_clock::now();
    updateCurrent(sessionId, conName);
    const auto endPoint = std::chrono::high_resolution_clock::now();
    const auto interval = std::chrono::duration_cast<std::chrono::milliseconds>(
                              endPoint - startPoint)
                              .count();
    emit updated(QStringLiteral("%1 page").arg(mPageName), interval);
  }
}
