//
// Created by Bond on 10/22/2021.
//

#ifndef DATABOND_DIAGRAM_H
#define DATABOND_DIAGRAM_H

#include <QDialog>


QT_BEGIN_NAMESPACE
namespace Ui { class DisplayDiagram; }
QT_END_NAMESPACE

class DisplayDiagram : public QDialog {
Q_OBJECT

public:
    explicit DisplayDiagram(const int sessionId, const QString& conName, const QStringList& tables, QWidget *parent = nullptr);

    ~DisplayDiagram() override;

private slots:
    void save();

private:
    void initDiagram();

private:
    Ui::DisplayDiagram *ui;
    QImage mImage;
    QStringList mTables;
    int mSessionId;
    QString mConName;
};


#endif //DATABOND_DIAGRAM_H
