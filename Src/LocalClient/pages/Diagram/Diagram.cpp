#include "Diagram.h"
#include "../utilities/LogWidget.hpp"
#include "ui_Diagram.h"
#include <LocalClient.hpp>
#include <QFileDialog>
#include <QImage>
#include <QMenu>
#include <QProcess>

class Node {
public:
  Node(const QString &name) : mName{name} {}

public:
  auto connectTo(const QSharedPointer<Node> node, const QString &labelName = "")
      -> void {
    mConnectedNodes.push_back({node, labelName});
  }

  auto toDot() const -> QString {
    QString output =
        QStringLiteral("\"%1\" [label = \"%1\", shape=ellipse];\n").arg(mName);
    for (const auto &node : mConnectedNodes) {
      output += QStringLiteral("\"%1\" -> \"%2\" [label =\"%3\"]\n")
                    .arg(mName)
                    .arg(node.first->mName)
                    .arg(node.second);
    }
    return output;
  }

  auto getName() const noexcept -> QString { return mName; }

private:
  QString mName;
  QList<QPair<QSharedPointer<Node>, QString>> mConnectedNodes;
};

class DotDocument {
public:
  DotDocument(const QString &name) : mName{name} {}

public:
  auto addNode(const QSharedPointer<Node> node) -> void {
    mNodes.push_back(node);
  }

  auto addNode(const QString &node) -> void {
    mNodes.push_back(QSharedPointer<Node>{new Node{node}});
  }

  auto toDot() const -> QString {
    QString output = QStringLiteral("digraph \"%1\" {\n"
                                    "node [\n"
                                    "    fontsize=\"%2\"\n"
                                    "];\n")
                         .arg(mName)
                         .arg(mFontSize);
    for (const auto &node : mNodes) {
      output += node->toDot();
    }
    output += QStringLiteral("}");
    return output;
  }

  auto connect(const QString &src, const QString &des,
               const QString &labelName = "") -> void {
    if (auto srcNode = node(src), desNode = node(des);
        !srcNode.isNull() && !desNode.isNull()) {
      srcNode->connectTo(desNode, labelName);
    }
  }

private:
  auto node(const QString &name) const -> QSharedPointer<Node> {
    for (const auto &node : mNodes) {
      if (node->getName() == name) {
        return node;
      }
    }
    return QSharedPointer<Node>{};
  }

private:
  QString mName;
  int mFontSize = 12;
  QList<QSharedPointer<Node>> mNodes;
};

DisplayDiagram::DisplayDiagram(const int sessionId, const QString &conName,
                               const QStringList &tables, QWidget *parent)
    : QDialog(parent), ui(new Ui::DisplayDiagram), mTables{tables},
      mSessionId{sessionId}, mConName{conName} {
  ui->setupUi(this);
  setWindowTitle("Tables Diagrams");

  ui->diagramsLabel->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this, &QDialog::rejected, this, &QDialog::deleteLater);
  initDiagram();
  connect(ui->diagramsLabel, &QLabel::customContextMenuRequested,
          [this](const QPoint &) {
            QMenu menu;
            QAction saveAction{"Save"};
            connect(&saveAction, &QAction::triggered, this,
                    &DisplayDiagram::save);
            menu.addAction(&saveAction);
            menu.exec(QCursor::pos());
          });
}

DisplayDiagram::~DisplayDiagram() { delete ui; }

void DisplayDiagram::save() {
  auto filename = QFileDialog::getSaveFileName(
      this, "Save png file",
      QStandardPaths::locate(QStandardPaths::HomeLocation, ".",
                             QStandardPaths::LocateDirectory),
      "Png file (*.png)");
  if (!filename.isEmpty()) {
    if (!filename.endsWith(".png")) {
      filename.append(".png");
    }
    if (mImage.save(filename)) {
      logInfo("Saved");
    } else {
      errorLog(QStringLiteral("Could not save in %1").arg(filename));
    }
  }
}

void DisplayDiagram::initDiagram() {
  DotDocument doc{"Tables"};
  for (const auto &table : mTables) {
    doc.addNode(table);
  }

  for (const auto &table : mTables) {
    GLOBAL_CLIENT
        ->call(&ConnectionManager::foreignKeys, mSessionId, mConName, table)
        .map([&table, &doc](const QList<QPair<QString, QString>> &fks) {
          for (const auto &fk : fks) {
            doc.connect(table, fk.second, fk.first);
          }
        });
  }
  QProcess process;
  process.start("dot", {"-Tsvg"});
  process.write(doc.toDot().toUtf8());
  process.closeWriteChannel();
  if (process.waitForFinished()) {
    const auto data = process.readAll();
    mImage.loadFromData(data);
    ui->diagramsLabel->setPixmap(QPixmap::fromImage(mImage));
    ui->diagramsLabel->setFixedSize(mImage.size() + mImage.size() * 1 / 10.0);
    setFixedSize(ui->diagramsLabel->size());
  } else {
    errorLog("Failed to start dot program:");
    errorLog(process.errorString());
    logInfo("Please make sure the dot program is installed correctly on your "
            "system or in "
            "it's in enviroment variables");
  }
}
