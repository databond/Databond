#include "SessionTreeModel.hpp"
#include <LocalClient.hpp>

#include <QDebug>
#include <QVariant>

namespace model {
using namespace Sessions;
using namespace Connections;
using namespace Control;
SessionTreeModel::SessionTreeModel(QObject *parent)
    : QStandardItemModel{parent} {
  connect(&GLOBAL_CLIENT->mSession, &SessionManager::nameChanged, this,
          &SessionTreeModel::changeName, Qt::QueuedConnection);

  connect(&GLOBAL_CLIENT->mSession, &SessionManager::sessionClosed, this,
          &SessionTreeModel::closeSession, Qt::QueuedConnection);

  connect(&GLOBAL_CLIENT->mSession, &SessionManager::sessionCreated, this,
          &SessionTreeModel::addSession, Qt::QueuedConnection);

  connect(&GLOBAL_CLIENT->mConnection, &ConnectionManager::connectionRegistered,
          this, &SessionTreeModel::addConnection, Qt::QueuedConnection);

  connect(&GLOBAL_CLIENT->mConnection, &ConnectionManager::connectionRemoved,
          this, &SessionTreeModel::removeConnection);

  setHorizontalHeaderLabels({"Name", "ID"});
}

void SessionTreeModel::addSession(int id) {
  const auto name = GLOBAL_CLIENT->call(&SessionManager::name, id).value();
  invisibleRootItem()->appendRow(
      QList{new QStandardItem{name}, new QStandardItem{QString::number(id)}});
}

void SessionTreeModel::closeSession(int id) {
  const auto items = findItems(QString::number(id), Qt::MatchExactly, 1);
  if (items.isEmpty()) {
    return;
  }
  const auto item = items[0];
  invisibleRootItem()->removeRow(item->row());
}

void SessionTreeModel::changeName(int id, QString name) {
  const auto items = findItems(QString::number(id), Qt::MatchExactly, 1);
  if (items.isEmpty()) {
    return;
  }
  const auto item = items.first();
  auto nameItem = invisibleRootItem()->child(item->row(), 0);
  nameItem->setData(name, Qt::DisplayRole);
}

void SessionTreeModel::addConnection(int id, QString name) {
  const auto items = findItems(QString::number(id), Qt::MatchExactly, 1);
  if (items.isEmpty()) {
    return;
  }
  const auto item = items[0];
  auto nameItem = invisibleRootItem()->child(item->row(), 0);
  nameItem->appendRow(new QStandardItem{name});
}

void SessionTreeModel::removeConnection(int id, QString name) {
  const auto items = findItems(QString::number(id), Qt::MatchExactly, 1);
  if (items.isEmpty()) {
    return;
  }
  const auto item = items[0];
  auto nameItem = invisibleRootItem()->child(item->row(), 0);
  const auto rowCount = nameItem->rowCount();
  for (auto i = 0; i < rowCount; ++i) {
    if (const auto conItem = nameItem->child(i, 0);
        conItem->data(Qt::DisplayRole).toString() == name) {
      nameItem->removeRow(i);
      break;
    }
  }
}

bool SessionTreeModel::setData(const QModelIndex &index, const QVariant &value,
                               int role) {
  if (role != Qt::DisplayRole) {
    return false;
  }
  return QStandardItemModel::setData(index, value, role);
}

} // namespace model
