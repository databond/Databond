#ifndef SESSIONTREEMODEL_H
#define SESSIONTREEMODEL_H

#include <QList>
#include <QMap>
#include <QObject>
#include <QPair>
#include <QStandardItemModel>

namespace model {
class SessionTreeModel : public QStandardItemModel {
  Q_OBJECT
  public:
  SessionTreeModel(QObject *parent = nullptr);
  // QAbstractItemModel interface
  public:
  virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;

  private slots:
  void addSession(int id);
  void closeSession(int id);
  void changeName(int id, QString name);
  void addConnection(int id, QString name);
  void removeConnection(int id, QString name);

  private:
};
} // namespace model

#endif // SESSIONTREEMODEL_H
