#ifndef PLUGINPAGE_HPP
#define PLUGINPAGE_HPP

#include <QWizardPage>
namespace Ui {
class PluginPage;
}

class PluginPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit PluginPage(QWidget *parent = nullptr);
    ~PluginPage();

private:
    Ui::PluginPage *ui;
};

#endif // PLUGINPAGE_HPP
