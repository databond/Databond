#ifndef CONNECTIONINFOPAGE_HPP
#define CONNECTIONINFOPAGE_HPP

#include <QWizardPage>

namespace Ui {
class ConnectionInfoPage;
}

class ConnectionInfoPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ConnectionInfoPage(QWidget *parent = nullptr);
    ~ConnectionInfoPage();

public:
    auto connectionName() const -> QString;
    auto username() const -> QString;
    auto password() const -> QString;
    auto host() const -> QString;
    auto port() const -> int;
    auto database() const -> QString;
    auto options() const -> QString;

private:
    Ui::ConnectionInfoPage *ui;

private:
    void initOptionsTable();
};

#endif // CONNECTIONINFOPAGE_HPP
