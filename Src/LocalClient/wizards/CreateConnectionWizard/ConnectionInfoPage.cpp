#include "ConnectionInfoPage.hpp"
#include "ui_ConnectionInfoPage.h"
#include <QIntValidator>
#include <QTreeWidgetItem>

ConnectionInfoPage::ConnectionInfoPage(QWidget *parent)
    : QWizardPage(parent), ui(new Ui::ConnectionInfoPage) {
  ui->setupUi(this);

  ui->portEdit->setValidator(new QIntValidator{0, 9999, this});
  registerField("conName*", ui->conNameEdit);
  registerField("username", ui->usernameEdit);
  registerField("password", ui->passwordEdit);
  registerField("host", ui->hostEdit);
  registerField("port", ui->portEdit);
  registerField("database*", ui->dbNameEdit);
}

ConnectionInfoPage::~ConnectionInfoPage() { delete ui; }
