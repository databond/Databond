#ifndef CREATECONNECTIONWIZARD_HPP
#define CREATECONNECTIONWIZARD_HPP

#include <QWizard>

class CreateConnectionWizard : public QWizard
{
    Q_OBJECT
public:
    CreateConnectionWizard(const int sessionId, const QString& conName, QWidget *parent = nullptr);

public:
    auto connectionName() const -> QString;
    auto username() const -> QString;
    auto password() const -> QString;
    auto host() const -> QString;
    auto port() const -> int;
    auto database() const -> QString;
    auto options() const -> QString;
    auto pluginName() const -> QString;

};

#endif // CREATECONNECTIONWIZARD_HPP
