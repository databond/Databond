#include "CreateConnectionWizard.hpp"
#include "ConnectionInfoPage.hpp"
#include "PluginPage.hpp"
#include <QVariant>

CreateConnectionWizard::CreateConnectionWizard(const int sessionId, const QString& conName, QWidget *parent)
    : QWizard{parent}
{
    addPage(new PluginPage);
    addPage(new ConnectionInfoPage);
}

QString CreateConnectionWizard::connectionName() const
{
    return field("conName").toString();
}

QString CreateConnectionWizard::username() const
{
    return field("username").toString();
}

QString CreateConnectionWizard::password() const
{
    return field("password").toString();
}

QString CreateConnectionWizard::host() const
{
    return field("host").toString();
}

int CreateConnectionWizard::port() const
{
    return field("port").toInt();
}

QString CreateConnectionWizard::database() const
{
    return field("database").toString();
}

QString CreateConnectionWizard::options() const
{
    return "";
}

QString CreateConnectionWizard::pluginName() const
{
    return field("PluginName").toString();
}
