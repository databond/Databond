#include "PluginPage.hpp"
#include "ui_PluginPage.h"
#include <QSqlDatabase>

PluginPage::PluginPage(QWidget *parent)
    : QWizardPage(parent), ui(new Ui::PluginPage) {
  ui->setupUi(this);
  ui->pluginComboBox->addItems(QSqlDatabase::drivers());
  registerField("PluginName", ui->pluginComboBox, "currentText",
                SIGNAL(currentTextChanged(QString)));
}

PluginPage::~PluginPage() { delete ui; }
