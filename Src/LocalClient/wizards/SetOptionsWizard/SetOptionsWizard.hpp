#ifndef SETOPTIONSWIZARD_HPP
#define SETOPTIONSWIZARD_HPP

#include <QWizard>

class SetOptionsWizard : public QWizard {
  Q_OBJECT
public:
  SetOptionsWizard(const int sessionId, const QString &conName,
                   QWidget *parent = nullptr);

public:
  auto options() const -> QString;
};

#endif // SETOPTIONSWIZARD_HPP
