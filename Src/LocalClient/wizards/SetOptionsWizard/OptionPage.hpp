#ifndef OPTIONPAGE_HPP
#define OPTIONPAGE_HPP

#include <QWizardPage>

namespace Ui {
class OptionPage;
}

class OptionPage : public QWizardPage {
  Q_OBJECT

public:
  explicit OptionPage(const int sessionId, const QString &conName,
                      QWidget *parent = nullptr);
  ~OptionPage();

private slots:
  void updateOptionsEdit();

private:
  Ui::OptionPage *ui;
};

#endif // OPTIONPAGE_HPP
