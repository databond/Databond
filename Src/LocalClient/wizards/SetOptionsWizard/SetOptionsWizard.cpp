#include "SetOptionsWizard.hpp"
#include "OptionPage.hpp"
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QWizardPage>

SetOptionsWizard::SetOptionsWizard(const int sessionId, const QString &conName,
                                   QWidget *parent)
    : QWizard{parent} {
  addPage(new OptionPage{sessionId, conName});
}

QString SetOptionsWizard::options() const {
  return field("options").toString();
}
