#include "OptionPage.hpp"
#include "ui_OptionPage.h"
#include <LocalClient.hpp>
#include <QTableWidgetItem>

OptionPage::OptionPage(const int sessionId, const QString &conName,
                       QWidget *parent)
    : QWizardPage{parent}, ui(new Ui::OptionPage) {
  ui->setupUi(this);
  ui->optionsEdit->setVisible(false);

  ui->optionsTable->horizontalHeader()->setSectionResizeMode(
      QHeaderView::Stretch);
  registerField("options", ui->optionsEdit);

  GLOBAL_CLIENT->call(&ConnectionManager::availableOptions, sessionId, conName)
      .map([this, sessionId, &conName](const QStringList &availableOptions) {
        const auto options = GLOBAL_CLIENT
                                 ->call(&ConnectionManager::connectionOptions,
                                        sessionId, conName)
                                 .value_or("")
                                 .split(";");
        QMap<QString, QString> optionsWithValue;
        for (const auto &optionValue : options) {
          const auto list = optionValue.split("=");
          optionsWithValue[list.first()] = list.last();
        }
        ui->optionsTable->setRowCount(availableOptions.count());
        for (qsizetype i = 0, size = availableOptions.count(); i < size; ++i) {
          auto item = new QTableWidgetItem{availableOptions[i]};
          QFlags flags{Qt::ItemIsEnabled};
          flags.setFlag(Qt::ItemIsEditable, false);
          flags.setFlag(Qt::ItemIsSelectable, false);
          item->setFlags(flags);
          ui->optionsTable->setItem(i, 0, item);
          ui->optionsTable->setItem(i, 1,
                                    new QTableWidgetItem{optionsWithValue.value(
                                        availableOptions[i], "")});
        }
        auto timer = new QTimer{this};
        connect(timer, &QTimer::timeout, this, &OptionPage::updateOptionsEdit);
        timer->start(100);
      });
}

OptionPage::~OptionPage() { delete ui; }

void OptionPage::updateOptionsEdit() {
  QString res;
  for (auto i = 0, size = ui->optionsTable->rowCount(); i < size; ++i) {
    const auto value = ui->optionsTable->item(i, 1)->text().trimmed();
    if (!value.isEmpty()) {
      const auto option = ui->optionsTable->item(i, 0)->text();
      res += QStringLiteral("%1=%2;").arg(option).arg(value);
    }
  }
  if (res.endsWith(';')) {
    res.remove(res.length() - 1, 1);
  }
  ui->optionsEdit->setText(res);
}
