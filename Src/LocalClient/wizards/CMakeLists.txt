add_subdirectory(CreateConnectionWizard)
add_subdirectory(SetOptionsWizard)

add_library(wizards)

target_link_libraries(wizards ConnectionWizard OptionsWizard Qt${QT_VERSION_MAJOR}::Widgets)
