#ifndef CONNECTIONPROPERTIESDIALOG_HPP
#define CONNECTIONPROPERTIESDIALOG_HPP

#include <QDialog>

namespace Ui {
class ConnectionPropertiesDialog;
}

class ConnectionPropertiesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConnectionPropertiesDialog(const int sessionId, const QString &conName, QWidget *parent = nullptr);
    ~ConnectionPropertiesDialog();

private:
    Ui::ConnectionPropertiesDialog *ui;
};

#endif // CONNECTIONPROPERTIESDIALOG_HPP
