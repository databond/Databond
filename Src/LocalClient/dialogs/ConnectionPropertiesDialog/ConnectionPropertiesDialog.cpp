#include "ConnectionPropertiesDialog.hpp"
#include "ui_ConnectionPropertiesDialog.h"
#include <LocalClient.hpp>

ConnectionPropertiesDialog::ConnectionPropertiesDialog(const int sessionId,
                                                       const QString &conName,
                                                       QWidget *parent)
    : QDialog(parent), ui(new Ui::ConnectionPropertiesDialog) {
  ui->setupUi(this);
  setWindowTitle("Connection Properties");
  ui->conNameLabel->setText(conName);
  ui->sessionIdLabel->setNum(sessionId);
  ui->databaseNameLabel->setText(
      GLOBAL_CLIENT->call(&ConnectionManager::databaseName, sessionId, conName)
          .value_or("N/A"));
  ui->hostnameLabel->setText(
      GLOBAL_CLIENT->call(&ConnectionManager::hostName, sessionId, conName)
          .value_or("N/A"));
  ui->portLabel->setNum(
      GLOBAL_CLIENT->call(&ConnectionManager::port, sessionId, conName)
          .value_or(-1));
  ui->pluginNameLabel->setText(
      GLOBAL_CLIENT->call(&ConnectionManager::plugin, sessionId, conName)
          .value_or("N/A"));
  ui->usernameLabel->setText(
      GLOBAL_CLIENT->call(&ConnectionManager::username, sessionId, conName)
          .value_or("N/A"));
}

ConnectionPropertiesDialog::~ConnectionPropertiesDialog() { delete ui; }
