#ifndef QUERYITEM_HPP
#define QUERYITEM_HPP

#include <QWidget>

namespace Ui {
class QueryItem;
}

class QueryItem : public QWidget {
  Q_OBJECT

public:
  explicit QueryItem(const QString &query, QWidget *parent = nullptr);
  ~QueryItem();
signals:
  void importRequested(QString);

private:
  Ui::QueryItem *ui;
};

#endif // QUERYITEM_HPP
