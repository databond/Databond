#include "QueryHistoryDialog.hpp"
#include "QueryItem.hpp"
#include "ui_QueryHistoryDialog.h"
#include <QDebug>
#include <QLabel>
#include <QSet>
#include <QSettings>

QueryHistoryDialog::QueryHistoryDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::QueryHistoryDialog) {
  ui->setupUi(this);
  setWindowTitle("Query History");

  QSettings settings;
  auto queries =
      settings.value("Databond/LocalClient/QueryHistory", QStringList{})
          .toStringList();

  for (auto iter = queries.crbegin(), end = queries.crend(); iter != end;
       ++iter) {
    addQuery(*iter);
  }
  connect(ui->clearBtn, &QPushButton::clicked, this,
          &QueryHistoryDialog::clearQueries);
}

QueryHistoryDialog::~QueryHistoryDialog() { delete ui; }

void QueryHistoryDialog::addQuery(const QString &query) {
  auto queryWidget = new QueryItem{query, this};

  connect(queryWidget, &QueryItem::importRequested, [this](QString query) {
    mImportedQuery = query;
    done(Accepted);
  });
  ui->queriesLabels->addWidget(queryWidget);
  mWidgets.append(queryWidget);
}

void QueryHistoryDialog::clearQueries() {
  for (const auto widget : qAsConst(mWidgets)) {
      widget->deleteLater();
    ui->queriesLabels->removeWidget(widget);
  }
  QSettings settings;
  settings.setValue("Databond/LocalClient/QueryHistory", QStringList{});
}

QString QueryHistoryDialog::getImportedQuery() { return mImportedQuery; }
