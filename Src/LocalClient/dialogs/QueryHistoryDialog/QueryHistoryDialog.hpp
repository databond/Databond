#ifndef QUERYHISTORYDIALOG_HPP
#define QUERYHISTORYDIALOG_HPP

#include <QDialog>

namespace Ui {
class QueryHistoryDialog;
}

class QueryHistoryDialog : public QDialog {
  Q_OBJECT

public:
  explicit QueryHistoryDialog(QWidget *parent = nullptr);
  ~QueryHistoryDialog();

private:
  auto addQuery(const QString &query) -> void;

private slots:
  void clearQueries();

public:
  auto getImportedQuery() -> QString;

private:
  Ui::QueryHistoryDialog *ui;
  QString mImportedQuery;
  QList<QWidget *> mWidgets;
};

#endif // QUERYHISTORYDIALOG_HPP
