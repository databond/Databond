#include "QueryItem.hpp"
#include "ui_QueryItem.h"
#include <QSettings>

inline auto removeFromSettings(const QVariant &v) -> void {
  QSettings settings;
  auto queries =
      settings.value("Databond/LocalClient/QueryHistory", {}).toStringList();
  queries.removeOne(v.toString());
  queries = QStringList{queries.cbegin(), queries.cend()};
  settings.setValue("Databond/LocalClient/QueryHistory", queries);
}

QueryItem::QueryItem(const QString &query, QWidget *parent)
    : QWidget(parent), ui(new Ui::QueryItem) {
  ui->setupUi(this);
  ui->queryLabel->setText(query);
  connect(ui->importBtn, &QPushButton::clicked,
          [this, query]() { emit importRequested(query); });
  connect(ui->removeBtn, &QPushButton::clicked, [this, query]() {
    removeFromSettings(query);
    deleteLater();
  });
}

QueryItem::~QueryItem() { delete ui; }
