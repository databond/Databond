#ifndef SERVICE_MESSAGEPACKRPCSERVER_HPP
#define SERVICE_MESSAGEPACKRPCSERVER_HPP

#include <QObject>
#include <QScopedPointer>
#include <rpc/server.h>
namespace rpc {
class server;
}
namespace Service {

class MessagePackRpcServer : public QObject {
  Q_OBJECT
public:
  explicit MessagePackRpcServer(const QString &add, const uint16_t port,
                                QObject *parent = nullptr);

public slots:
  void start(const quint16 threadsNumber = 1);

private:
  void initMethodsBinding();
  void initSessionMethodsBinding();
  void initConnectionMethodsBinding();
  void initLoggingMethodsBinding();
  QString currentSession();
signals:

private:
  QScopedPointer<rpc::server> mServer;
};

} // namespace Service

#endif // SERVICE_MESSAGEPACKRPCSERVER_HPP
