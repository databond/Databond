#include "MessagePackRpcServer.hpp"
#include "Types.hpp"
#include "qhostaddress.h"
#include "qtcpsocket.h"
#include <Control/Client.hpp>
#include <Control/ClientManager.hpp>
#include <QFile>
#include <QJsonObject>
#include <QTcpSocket>
#include <rpc/config.h>
#include <rpc/this_handler.h>
#include <rpc/this_session.h>

using namespace Control;
using namespace Sessions;
using namespace Connections;
using namespace Log;
namespace Service {

MessagePackRpcServer::MessagePackRpcServer(const QString &add,
                                           const uint16_t port, QObject *parent)
    : QObject(parent), mServer{new rpc::server{add.toStdString(), port}} {
  initMethodsBinding();
}

void MessagePackRpcServer::start(const quint16 threadsNumber) {
  mServer->suppress_exceptions(true);
  mServer->async_run(threadsNumber);
}
void MessagePackRpcServer::initMethodsBinding() {
  initSessionMethodsBinding();
  initConnectionMethodsBinding();
  initLoggingMethodsBinding();
}
void MessagePackRpcServer::initLoggingMethodsBinding() {
  mServer->bind("log.info", [this](const std::string &msg) {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::log<QString>, QString::fromStdString(msg) + '\n',
                      LogType::Info);
    }
  });
  mServer->bind("log.warn", [this](const std::string &msg) {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::log<QString>, QString::fromStdString(msg) + '\n',
                      LogType::Warn);
    }
  });
  mServer->bind("log.error", [this](const std::string &msg) {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::log<QString>, QString::fromStdString(msg) + '\n',
                      LogType::Error);
    }
  });
  mServer->bind("log.device.filename", [this](const std::string &filename) {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::setDevice,
                      new QFile{QString::fromStdString(filename)});
    }
  });
  mServer->bind(
      "log.device.socket", [this](const std::string &ip, const int port) {
        if (auto c = ClientManager::client(currentSession()); c.has_value()) {
          c.value()->call(&Logger::setSocketDevice, QString::fromStdString(ip),
                          port, 3000);
        }
      });
  mServer->bind("log.enable", [this]() {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::setEnabled, true);
    }
  });
  mServer->bind("log.disable", [this]() {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      c.value()->call(&Logger::setEnabled, false);
    }
  });
  mServer->bind("log.lastError", [this]() -> std::string {
    if (auto c = ClientManager::client(currentSession()); c.has_value()) {
      return c.value()->call(&Logger::lastError).toStdString();
    } else {
      rpc::this_handler().respond_error(Error{c.error()});
    }
    return "";
  });
}
void MessagePackRpcServer::initSessionMethodsBinding() {
  mServer->bind("request", [this]() -> bool {
    if (auto c = ClientManager::request(currentSession()); c.has_value()) {
      return true;
    } else {
      rpc::this_handler().respond_error(Error{c.error()});
    }
    return false;
  });
  mServer->bind(
      "session.list", [this]() -> std::vector<std::pair<int, std::string>> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          std::vector<std::pair<int, std::string>> s;
          auto sessions = client.value()->call(&SessionManager::sessions);
          for (const auto session : sessions) {
            s.push_back({session.first, session.second.toStdString()});
          }
          return s;
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind("session.setName",
                [this](const int id, const std::string &name) {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    client.value()->call(&SessionManager::setName, id,
                                         QString::fromStdString(name));
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                });
  mServer->bind("session.new", [this]() -> int {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      return client.value()->call(QOverload<>::of(&SessionManager::newSession));
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return -1;
  });

  mServer->bind("session.newWithName", [this](const std::string &name) -> int {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      return client.value()->call(
          QOverload<const QString &>::of(&SessionManager::newSession),
          QString::fromStdString(name));
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return -1;
  });

  mServer->bind("session.exist", [this](const int id) -> bool {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      return client.value()->call(&SessionManager::exists, id);
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return false;
  });
  mServer->bind("session.count", [this]() -> int {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      return client.value()->call(&SessionManager::count);
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return -1;
  });
  mServer->bind("session.close", [this](const int id) {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      return client.value()->call(&SessionManager::close, id);
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
  });
  mServer->bind("session.name", [this](const int id) -> std::string {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      auto res = client.value()->call(&SessionManager::name, id);
      if (res.has_value()) {
        return res.value().toStdString();
      } else {
        rpc::this_handler().respond_error(Error{res.error()});
      }
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return "";
  });

  mServer->bind("session.file.save",
                [this](const int id, const std::string &filename) -> bool {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&SessionManager::saveInFile, id,
                                             QString::fromStdString(filename));
                    if (res.has_value()) {
                      return true;
                    } else {
                      rpc::this_handler().respond_error(Error{res.error()});
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return false;
                });
  mServer->bind("session.file.open", [this](const std::string &name) -> int {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      auto res = client.value()->call(
          &SessionManager::newSessionFromFile<Connections::DBConnection>,
          QString::fromStdString(name), &client.value()->mConnection);
      if (res.has_value()) {
        return res.value();
      } else {
        rpc::this_handler().respond_error(Error{res.error()});
      }
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return -1;
  });
}

void MessagePackRpcServer::initConnectionMethodsBinding() {
  using namespace std;
  mServer->bind("connection.addConnection", [this](const int id,
                                                   const string &pluginName,
                                                   const std::string &conName) {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      client.value()->call(
          QOverload<const int, const QString &, const QString &>::of(
              &ConnectionManager::addConnection),
          id, QString::fromStdString(pluginName),
          QString::fromStdString(conName));
    }
  });
  mServer->bind(
      "connection.list", [this](const int id) -> std::vector<std::string> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          std::vector<std::string> connections;
          const auto cons =
              client.value()->call(&ConnectionManager::connectionNames, id);
          for (const auto &con : cons) {
            connections.push_back(con.toStdString());
          }
          return connections;
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });

  mServer->bind("connection.exist",
                [this](const int id, const string &conName) -> bool {
                  auto client = ClientManager::client(currentSession());
                  return client.has_value() &&
                         client.value()->call(&ConnectionManager::exist, id,
                                              QString::fromStdString(conName));
                });
  mServer->bind("connection.pluginName",
                [this](const int id, const string &conName) -> std::string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::plugin, id,
                                             QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind("connection.availablePlugins", [this]() -> std::vector<string> {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      const auto plugins = QSqlDatabase::drivers();
      std::vector<string> ps;
      for (const auto &plugin : plugins) {
        ps.push_back(plugin.toStdString());
      }
      return ps;
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return {};
  });
  mServer->bind(
      "connection.open", [this](const int id, const string &conName) -> bool {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res =
              client.value()->call(QOverload<const int, const QString &>::of(
                                       &ConnectionManager::open),
                                   id, QString::fromStdString(conName));
          if (res.has_value()) {
            return true;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return false;
      });
  mServer->bind("connection.count", [this](const int id) -> int {
    if (auto client = ClientManager::client(currentSession());
        client.has_value()) {
      auto res = client.value()->call(&ConnectionManager::count, id);
      if (res.has_value()) {
        return res.value();
      } else {
        rpc::this_handler().respond_error(Error{res.error()});
      }
    } else {
      rpc::this_handler().respond_error(Error{client.error()});
    }
    return -1;
  });
  mServer->bind(
      "connection.commit", [this](const int id, const string &conName) -> bool {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::commit, id,
                                          QString::fromStdString(conName));
          if (res.has_value()) {
            return true;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return false;
      });
  mServer->bind("connection.transaction",
                [this](const int id, const string &conName) -> bool {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res = client.value()->call(
                        &ConnectionManager::transaction, id,
                        QString::fromStdString(conName));
                    if (res.has_value()) {
                      return true;
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return false;
                });
  mServer->bind("connection.rollback",
                [this](const int id, const string &conName) -> bool {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::rollback, id,
                                             QString::fromStdString(conName));
                    if (res.has_value()) {
                      return true;
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return false;
                });
  mServer->bind(
      "connection.isOpen", [this](const int id, const string &conName) -> bool {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::isOpen, id,
                                          QString::fromStdString(conName));
          if (res.has_value()) {
            return res.value();
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return false;
      });
  mServer->bind("connection.close",
                [this](const int id, const string &conName) {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    client.value()->call(&ConnectionManager::close, id,
                                         QString::fromStdString(conName));
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                });
  mServer->bind("connection.options",
                [this](const int id, const string &conName) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res = client.value()->call(
                        &ConnectionManager::connectionOptions, id,
                        QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind("connection.databaseName",
                [this](const int id, const string &conName) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res = client.value()->call(
                        &ConnectionManager::databaseName, id,
                        QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                      rpc::this_handler().respond_error(
                          Error{res.error().value<Sessions::SessionError>()});
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind("connection.execute",
                [this](const int id, const string &conName,
                       const string &query) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::exec, id,
                                             QString::fromStdString(conName),
                                             QString::fromStdString(query));
                    if (res.has_value()) {
                      return QJsonDocument::fromVariant(
                                 res.value().toJsonObject().toVariantMap())
                          .toJson(QJsonDocument::Compact)
                          .toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                      rpc::this_handler().respond_error(
                          Error{res.error().value<Sessions::SessionError>()});
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return {};
                });
  mServer->bind("connection.hostname",
                [this](const int id, const string &conName) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::hostName, id,
                                             QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind("connection.lastSqlError",
                [this](const int id, const string &conName) -> SqlError {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res = client.value()->call(
                        &ConnectionManager::lastSqlError, id,
                        QString::fromStdString(conName));
                    if (res.has_value()) {
                      return SqlError{res.value()};
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return SqlError{};
                });
  mServer->bind("connection.password",
                [this](const int id, const string &conName) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::password, id,
                                             QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind(
      "connection.port", [this](const int id, const string &conName) -> int {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::port, id,
                                          QString::fromStdString(conName));
          if (res.has_value()) {
            return res.value();
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return -1;
      });
  mServer->bind(
      "connection.setOptions",
      [this](const int id, const string &conName, const string &options) {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          client.value()->call(&ConnectionManager::setConnectionOptions, id,
                               QString::fromStdString(conName),
                               QString::fromStdString(options));
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
      });
  mServer->bind(
      "connection.setDatabaseName",
      [this](const int id, const string &conName, const string &database) {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          client.value()->call(&ConnectionManager::setDatabaseName, id,
                               QString::fromStdString(conName),
                               QString::fromStdString(database));
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
      });
  mServer->bind(
      "connection.setHostname",
      [this](const int id, const string &conName, const string &hostname) {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          client.value()->call(&ConnectionManager::setHostName, id,
                               QString::fromStdString(conName),
                               QString::fromStdString(hostname));
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
      });
  mServer->bind(
      "connection.setPassword",
      [this](const int id, const string &conName, const string &password) {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          client.value()->call(&ConnectionManager::setPassword, id,
                               QString::fromStdString(conName),
                               QString::fromStdString(password));
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
      });
  mServer->bind("connection.setPort",
                [this](const int id, const string &conName, const int port) {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    client.value()->call(&ConnectionManager::setPort, id,
                                         QString::fromStdString(conName), port);
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                });
  mServer->bind(
      "connection.setUsername",
      [this](const int id, const string &conName, const string &username) {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          client.value()->call(&ConnectionManager::setUsername, id,
                               QString::fromStdString(conName),
                               QString::fromStdString(username));
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
      });
  mServer->bind("connection.tables",
                [this](const int id, const string &conName,
                       const string &type) -> std::vector<string> {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::tables, id,
                                             QString::fromStdString(conName),
                                             QString::fromStdString(type));
                    if (res.has_value()) {
                      std::vector<string> tables;
                      for (const auto &table : res.value()) {
                        tables.push_back(table.toStdString());
                      }
                      return tables;
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return {};
                });
  mServer->bind("connection.username",
                [this](const int id, const string &conName) -> string {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res =
                        client.value()->call(&ConnectionManager::username, id,
                                             QString::fromStdString(conName));
                    if (res.has_value()) {
                      return res.value().toStdString();
                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return "";
                });
  mServer->bind("connection.columns",
                [this](const int id, const string &conName,
                       const string &tablenName) -> std::vector<string> {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    auto res = client.value()->call(
                        &ConnectionManager::columns, id,
                        QString::fromStdString(conName),
                        QString::fromStdString(tablenName));
                    if (res.has_value()) {
                      std::vector<string> c;
                      for (const auto &column : res.value()) {
                        c.push_back(column.toStdString());
                      }
                      return c;

                    } else {
                      auto error = res.error();
                      if (error.canConvert<Sessions::SessionError>()) {
                        rpc::this_handler().respond_error(
                            Error{error.value<Sessions::SessionError>()});
                      } else {
                        rpc::this_handler().respond_error(
                            Error{error.value<Connections::ConnectionError>()});
                      }
                    }
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                  return {};
                });
  mServer->bind(
      "connection.columnsWithTypes",
      [this](const int id, const string &conName,
             const string &tableName) -> std::vector<std::map<string, string>> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::columnsWithTypes,
                                          id, QString::fromStdString(conName),
                                          QString::fromStdString(tableName));
          if (res.has_value()) {
            std::vector<std::map<string, string>> c;
            for (const auto &column : res.value()) {
              c.push_back(
                  {{column.first.toStdString(), column.second.toStdString()}});
            }
            return c;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind(
      "connection.types",
      [this](const int id, const string &conName) -> std::vector<string> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::types, id,
                                          QString::fromStdString(conName));
          if (res.has_value()) {
            std::vector<string> t;
            for (const auto &type : res.value()) {
              t.push_back(type.toStdString());
            }
            return t;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind(
      "connection.availableOptions",
      [this](const int id, const string &conName) -> std::vector<string> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::availableOptions,
                                          id, QString::fromStdString(conName));
          if (res.has_value()) {
            std::vector<string> options;
            for (const auto &option : res.value()) {
              options.push_back(option.toStdString());
            }
            return options;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind(
      "connection.triggers",
      [this](const int id, const string &conName) -> std::vector<string> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::triggers, id,
                                          QString::fromStdString(conName));
          if (res.has_value()) {
            std::vector<string> t;
            for (const auto &trigger : res.value()) {
              t.push_back(trigger.toStdString());
            }
            return t;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind(
      "connection.fk",
      [this](const int id, const string &conName,
             const string &tableName) -> std::vector<std::map<string, string>> {
        if (auto client = ClientManager::client(currentSession());
            client.has_value()) {
          auto res = client.value()->call(&ConnectionManager::foreignKeys, id,
                                          QString::fromStdString(conName),
                                          QString::fromStdString(tableName));
          if (res.has_value()) {
            std::vector<std::map<string, string>> fks;
            for (const auto &fk : res.value()) {
              fks.push_back(
                  {{fk.first.toStdString(), fk.second.toStdString()}});
            }
            return fks;
          } else {
            auto error = res.error();
            if (error.canConvert<Sessions::SessionError>()) {
              rpc::this_handler().respond_error(
                  Error{error.value<Sessions::SessionError>()});
            } else {
              rpc::this_handler().respond_error(
                  Error{error.value<Connections::ConnectionError>()});
            }
          }
        } else {
          rpc::this_handler().respond_error(Error{client.error()});
        }
        return {};
      });
  mServer->bind("connection.remove",
                [this](const int id, const string &conName) {
                  if (auto client = ClientManager::client(currentSession());
                      client.has_value()) {
                    client.value()->call(&ConnectionManager::removeConnection,
                                         id, QString::fromStdString(conName));
                  } else {
                    rpc::this_handler().respond_error(Error{client.error()});
                  }
                });
}

QString MessagePackRpcServer::currentSession() {
  return QString::number(rpc::this_session().id());
}
} // namespace Service
