#ifndef TYPES_HPP
#define TYPES_HPP
#include <Connections/ConnectionError.hpp>
#include <Control/ClientError.hpp>
#include <QSqlError>
#include <QString>
#include <Sessions/SessionError.hpp>
#include <rpc/msgpack.hpp>

namespace Service {

struct Error {
  Error(const Connections::ConnectionError &er)
      : type{er.errorTag().toStdString()}, msg{er.explain().toStdString()} {}
  Error(const Sessions::SessionError &er)
      : type{er.errorTag().toStdString()}, msg{er.explain().toStdString()} {}
  Error(const Control::ClientError &er)
      : type{er.errorTag().toStdString()}, msg{er.explain().toStdString()} {}
  Error() = default;
  std::string type;
  std::string msg;
  MSGPACK_DEFINE_ARRAY(type, msg)
};

struct SqlError {
  SqlError() = default;
  SqlError(const SqlError &) = default;
  SqlError(const QSqlError &er)
      : errorCode{er.nativeErrorCode().toStdString()},
        text{er.text().toStdString()} {
    switch (er.type()) {
    case QSqlError::NoError:
      type = "NoError";
      break;
    case QSqlError::ConnectionError:
      type = "ConnectionError";
      break;
    case QSqlError::StatementError:
      type = "StatementError";
      break;
    case QSqlError::TransactionError:
      type = "TransactionError";
      break;
    case QSqlError::UnknownError:
      type = "UnknownError";
      break;
    }
  }
  std::string errorCode;
  std::string type;
  std::string text;
  MSGPACK_DEFINE_ARRAY(errorCode, type, text)
};

} // namespace Service
#endif // TYPES_HPP
