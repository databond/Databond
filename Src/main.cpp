#include <Control/ClientManager.hpp>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QHostAddress>
#include <Service/MessagePackRpc/MessagePackRpcServer.hpp>
#include <thread>
auto main(int argc, char *args[]) -> int {
  QCoreApplication a{argc, args};
  QCommandLineParser cmd;
  cmd.setApplicationDescription(
      "Tool that gives a unified interface to manage heterogeneous DBMSs");
  cmd.addOption({"address", "IPV4 address to listen to", "address",
                 QHostAddress{QHostAddress::LocalHost}.toString()});
  cmd.addOption({"port", "Listen port", "port", QString::number(8080)});
  cmd.addOption({"client-limit",
                 "Set the client limit(Negative numbers indidcates unlimited "
                 "number of clients)",
                 "number", "-1"});

  cmd.addOption(
      {"jobs-number",
       "Allow the server to operate with N jobs(If N is less than 1 the"
       "server will decide according to the available hardware "
       "concurrency - default)",
       "number", "-1"});
  cmd.addVersionOption();
  cmd.addHelpOption();
  cmd.process(a);
  QHostAddress add{cmd.value("address")};

  Service::MessagePackRpcServer server{
      add.toString(), static_cast<uint16_t>(cmd.value("port").toInt())};
  Control::ClientManager::setLimit(cmd.value("client-limit").toInt());
  auto jobsNumber = cmd.value("jobs-number").toInt();
  jobsNumber =
      jobsNumber < 1 ? std::thread::hardware_concurrency() : jobsNumber;
  try {
    server.start(jobsNumber);
  } catch (...) {
    qCritical("error at the server's parameter");
    server.start(jobsNumber);
  }

  return a.exec();
}
