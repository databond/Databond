# Databond

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Supported Interfaces](#supported_interfaces)

## About <a name = "about"></a>
Databond is a tool that manages and organizes different Database connections, through implementing their specific drivers.
You can use it to create your own client tool to a database, integrate it with your own application for other purposes, through communications interfaces

## Getting Started <a name = "getting_started"></a>

### Prerequisites
    - Qt 6.2.0 Is better but Qt 5.15 might work
    - Cmake 3.15 or higher
    - C++17 compiler support, Tested on MinGW GCC 11.2
    - Git
    - Make tool


### Installing

The installation process is common for all three major OSes(Windows, Linux, MacOS)
```
git clone https://gitlab.com/databond/Databond.git
cd Databond
git submodule update --init --recursive
mkdir build
cmake .. # Some other configurations are possible
make
make install #(Optional) To install globally on the system, might need an adminstration permssions
```

There are other configurations options that you can provide to cmake with -D < Option >=ON|OFF, they are OFF by default, you can't enable child configuration if its parents are off:

    Options:
    - LOCAL_CLIENT:                     Build Local Client
    - ENABLE_TESTS:                     Build Tests
        - Connections_Tests             Build connections tests binary
        - Sessions_Tests                Build sessions tests binary
        - Client_Tests                  Build Client tests
            - RpcRequests_Tests         Build Rpc requests tests binary
            - SessionManager_Tests      Build session manager tests binary
            - ConnectionManager_Tests   Build connection manager tests binary



## Usage <a name = "usage"></a>
Databond comes with two programs
- ### Databond service
    It's a command line tool that initiates the server that manages the DBMSs connections, it provides the following arguments with the format < Options > < Arguement >:

        --address < IPV4 >:              IPV4 address to listen to(Default is set to local host)
        --port < Number >:               Listen port(Default is 8080)
        --client-limit < Number >:       Set the client limit(Negative numbers indidcates unlimited number of clients - default)
        --jobs-number < Number >:        Allow the server to operate with N jobs(If N less than 1 the server will decide according to the available hardware concurrency - default).
        --help:                          Displays help on commandline options.

- ### Local client
    It's a GUI program that facilitates a basic usage fo some functionalities provided by Databond, it depends on Databond some internel modules.
    It has features like:
        
        - Sessions/Connections tree
        - Displaying Editable Tables for Database
        - Generate Diagram and relations between Tables
        - Customizable Sql Editor(See *Customizing Sql Editor* section)
        - View Triggers List of the database
        - Add Sessions and save them to specified (*.bond) files
        - Connections functionalities(Add, open, close... etc)
        - Movable Logging bar
        - Query History

    - #### Customizing Sql Editor:
        To style the Sql Editor for a specific Plugin(Database driver), you need to create a JSON file with the name of the plugin suffixed with Style.json, like SqliteStyle.json.
        Save the file under the _styles_ directory in the Local Client installation path.
        The Json file should comply to the following schema:
            

            {
                "CaseSensitive" : true | false // Apply case sensitivity to identify keywords
                "Sheet" : {
                    "Color" : "color", // Default color for the editor
                    "Size" : size, // Default font size
                    "QuotedText-Color" : "color", // The color for the quoted text
                    "FontStyle" : "style" // The default style (Either regular, italic, bold)
                    "Keywords" : [ // To style specific keywords
                        {
                            "Name" : "name" // The keyword that will be styled(Update, Select..etc)
                            "Color" : "color", // The color of the keyword
                            "Size" : size, // The font size of the keyword,
                            "FontStyle" : "style" // The font style of the keyword
                        },
                        ...
                    ]
                }
            }
        
        In case a plugin style not provided, a Default Style for standard SQL is loaded.
            
        

## Supported Interfaces <a name = "supported_interfaces"></a>

- ### Rpc MessagePack protocol
    Interfaces are explained in details in the wiki page
